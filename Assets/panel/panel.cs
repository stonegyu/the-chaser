using UnityEngine;
using System.Collections;

public class panel : MonoBehaviour {
	public GameObject[] Allconver;
	public int showConversation = 1;
	public bool showing = true;
	public bool showAll = false;
	public bool exit = false;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetMouseButtonDown(0) && showing == true && showAll == false){
			showAll = true;
		}
		if(Input.GetMouseButtonDown(0) && showing == false && exit == false){
			exit = true;
		}
		
		if(panel_camera.PC.PC_Layer.HitTest(Input.mousePosition) != null && panel_camera.PC.PC_Layer.HitTest(Input.mousePosition).tag == "skip" &&
			Input.GetMouseButtonDown(0))
			Destroy(gameObject);
			
		
		if(showConversation > Allconver.Length)
			Destroy(gameObject);
	
	}
}
