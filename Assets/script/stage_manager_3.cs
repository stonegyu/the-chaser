using UnityEngine;
using System.Collections;

public class stage_manager_3 : MonoBehaviour {
	public static stage_manager_3 stage;
	GameObject mission1Conver;
	GameObject mission2Conver;
	bool mission_1 = false,mission_2 = false;
	GameObject carOne;
	GameObject car;
	void Awake()
	{
		stage = this;
		if(Application.loadedLevelName == "scene_3"){
			PlayerPrefs.SetInt("Scene_number",3);
			GameObject.FindWithTag("mission").GetComponent<mission_move>().guiTexture.enabled = true;
			GameObject.FindWithTag("mission").GetComponent<mission_move>().guiTexture.texture = (Texture)Resources.Load("Mission(Stage1)");
		}
		if(Application.loadedLevelName == "scene_4"){
			PlayerPrefs.SetInt("Scene_number",4);
			GameObject.FindWithTag("mission").GetComponent<mission_move>().guiTexture.enabled = true;
			GameObject.FindWithTag("mission").GetComponent<mission_move>().guiTexture.texture = (Texture)Resources.Load("Mission(Stage1)");
		}
		
	}
	// Use this for initialization
	void Start () {
		mission1Conver = (GameObject)Resources.Load("stage3Mission1Conver");
		mission2Conver = (GameObject)Resources.Load("stage4Mission1Conver");
		StartCoroutine(handleSomething());
		carOne = (GameObject)Resources.Load("stage3carOne_1");
		car = (GameObject)Resources.Load("stage3car_1");
	}
	
	// Update is called once per frame
	void Update () {
		if(mission_move.MM != null && instance_outReck.ins_out != null && GameObject.FindWithTag("gamemanager").GetComponent<gamemanager>().loading_camera_on == false && instance_outReck.ins_out.exit_pushed == false &&
			gameover.over.end_game == false && mission_move.MM.guiTexture.enabled == false && GameObject.FindWithTag("one") == null){
			//Time.timeScale = 1.0f;
			GameObject.FindWithTag("gamemanager").GetComponent<gamemanager>().car_move_admit = true;
			pointer.PT.bPause = false;
			if(instance_outReck.ins_out != null)
				instance_outReck.ins_out.re_move();
		}
		else{
			GameObject.FindWithTag("gamemanager").GetComponent<gamemanager>().car_move_admit = false;
			//Time.timeScale =0.0f;
			if(instance_outReck.ins_out != null)
				instance_outReck.ins_out.stop_move();
		}
		
		if(Application.loadedLevelName == "scene_3"){
			if(mission_move.MM.guiTexture.enabled == false)
			{	
				if(gamemanager.gm._obj != null && gamemanager.gm._obj.name == "site_target" && gamemanager.gm.collision_stage == true)
				{
					Instantiate(mission1Conver,new Vector3(0,0,0),Quaternion.identity);
					mission_move.MM.guiTexture.texture = (Texture)Resources.Load("Mission(Stage3)");
					mission_move.MM.guiTexture.enabled = true;
					gamemanager.gm.collision_stage = false;
					Destroy(GameObject.Find("site_target"));
					GameObject.Find("arrow_2").renderer.enabled = true;
					GameObject.Find("out").collider.isTrigger = true;
					GameObject.Find("mini_out_target").renderer.enabled = true;
					mission_1 = true;
					Vector3 temp;
					temp = GameObject.Find("instance").transform.position;
					GameObject.Find("instance").transform.position = GameObject.Find("target_1(Clone)").transform.position;
					GameObject.Find("target_1(Clone)").transform.position = temp;
					Instantiate(carOne,new Vector3(-89,0,120),Quaternion.identity);
					Instantiate(car,new Vector3(-60,0,120),Quaternion.identity);
				}
				if(gamemanager.gm._obj != null && gamemanager.gm._obj.name == "out" && gamemanager.gm.collision_stage == true && mission_1 == true)
				{
					gamemanager gm = GameObject.FindWithTag("gamemanager").GetComponent<gamemanager>();
					gm.get_load_name = "scene_4";
				}
			}
		}
		if(Application.loadedLevelName == "scene_4")
		{
			if(mission_move.MM.guiTexture.enabled == false)
			{
				if(gamemanager.gm._obj != null && gamemanager.gm._obj.name == "target" && gamemanager.gm.collision_stage == true && mission_2 == false)
				{
					mission_move.MM.guiTexture.texture = (Texture)Resources.Load("Mission(Stage4)");
					Instantiate(mission2Conver,new Vector3(0,0,0),Quaternion.identity);
					mission_move.MM.guiTexture.enabled = true;
					gamemanager.gm.collision_stage = false;
					mission_2 = true;
				}
				if(gamemanager.gm._obj != null && gamemanager.gm._obj.name == "target" && mission_move.MM.guiTexture.enabled == false && mission_2 == true)
				{
					GameObject.FindWithTag("gamemanager").GetComponent<gamemanager>().get_load_name = "scene_5";
				}
				if(GameObject.Find("stage3carOne_1") == null)
					gameover.over.end_game = true;
			}
		}
	
	}
	
	IEnumerator handleSomething()
	{
		yield return new WaitForSeconds( 0.5f );
		GameObject.FindWithTag("gamemanager").GetComponent<gamemanager>().car_target_player = false;
	}
}
