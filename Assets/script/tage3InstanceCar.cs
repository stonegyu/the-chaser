using UnityEngine;
using System.Collections;

public class tage3InstanceCar : MonoBehaviour {
	GameObject line_1,line_2,line_3,line_4,line_5;
	public GameObject car_1;
	int rand_line;
	int count;
	float time;
	GameObject line;
	void Awake()
	{
		time =0;
	}
	// Use this for initialization
	void Start () {
		line_1 = GameObject.Find("line_1");
		line_2 = GameObject.Find("line_2");
		line_3 = GameObject.Find("line_3");
		line_4 = GameObject.Find("line_4");
		line_5 = GameObject.Find("line_5");
	}
	
	// Update is called once per frame
	void Update () {
		if(mission_move.MM.guiTexture.enabled == false){
			if(time < 21)
				time = time+Time.deltaTime;
			if(count <3 && time >20)
			{
				rand_line = Random.Range(0,5);
				if(rand_line == 0)
					line = line_1;
				else if(rand_line == 1)
					line = line_2;
				else if(rand_line == 2)
					line = line_3;
				else if(rand_line == 3)
					line = line_4;
				else if(rand_line == 4)
					line = line_5;
				Instantiate(car_1,line.transform.position,Quaternion.identity);
				count +=1;
			}
		}
	}
}
