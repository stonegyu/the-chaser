using UnityEngine;
using System.Collections;

public class parking_target : MonoBehaviour {
	public bool enter = false;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	}
	
	 void OnTriggerStay(Collider other) {
		if(other.transform.gameObject.name == "Player")
		{
			enter = true;
		}
	}
	
	void OnTriggerExit(Collider other) {
		if(other.transform.gameObject.name == "Player")
		{
			enter = false;
		}
	}
}
