using UnityEngine;
using System.Collections;

public class stage_manager_9 : MonoBehaviour {

	public static stage_manager_9 stage;
	GameObject mission1Conver,mission2Conver;
	GameObject missionLastConver;
	bool mission_1 = false,mission_2 = false,missionLast = false;
	GameObject car;
	bool one = false;
	void Awake()
	{
		stage = this;
		if(Application.loadedLevelName == "scene_9"){
			PlayerPrefs.SetInt("Scene_number",9);
		}
		if(Application.loadedLevelName == "scene_10"){
			PlayerPrefs.SetInt("Scene_number",10);
		}
		
	}
	// Use this for initialization
	void Start () {
		mission1Conver = (GameObject)Resources.Load("stage9Mission1Conver");
		mission2Conver = (GameObject)Resources.Load("stage9Mission2Conver");
		missionLastConver = (GameObject)Resources.Load("stage10Mission1Conver");
		car = (GameObject)Resources.Load("lastCar");
		StartCoroutine(handleSomething());
	}
	
	// Update is called once per frame
	void Update () {
		if(mission_move.MM != null && instance_outReck.ins_out != null && GameObject.FindWithTag("gamemanager").GetComponent<gamemanager>().loading_camera_on == false && instance_outReck.ins_out.exit_pushed == false &&
			gameover.over.end_game == false && mission_move.MM.guiTexture.enabled == false && GameObject.FindWithTag("one") == null){
			//Time.timeScale = 1.0f;
			GameObject.FindWithTag("gamemanager").GetComponent<gamemanager>().car_move_admit = true;
			pointer.PT.bPause = false;
			if(instance_outReck.ins_out != null)
				instance_outReck.ins_out.re_move();
		}
		else{
			GameObject.FindWithTag("gamemanager").GetComponent<gamemanager>().car_move_admit = false;
			//Time.timeScale =0.0f;
			if(instance_outReck.ins_out != null)
				instance_outReck.ins_out.stop_move();
		}
		
		if(Application.loadedLevelName == "scene_9"){
			if(mission_move.MM.guiTexture.enabled == false)
			{	
				if(gamemanager.gm._obj != null && gamemanager.gm._obj.name == "choi_target" && gamemanager.gm.collision_stage == true && mission_1 == false)
				{
					Instantiate(mission1Conver,new Vector3(0,0,0),Quaternion.identity);
					GameObject.FindWithTag("mission").GetComponent<mission_move>().guiTexture.texture = (Texture)Resources.Load("Mission(Stage9_1)");
					mission_move.MM.guiTexture.enabled = true;
					gamemanager.gm.collision_stage = false;
					Destroy(GameObject.Find("choi_target"));
					GameObject.Find("arrow_2").renderer.enabled = true;
					GameObject.Find("law_line").renderer.enabled = true;
					mission_1 = true;
					GameObject.Find("instance_car").GetComponent<lastStageIntanceCar>().instanceCar = true;
				}
				if(gamemanager.gm._obj != null && gamemanager.gm._obj.name == "law_target" && gamemanager.gm.collision_stage == true && mission_1 == true)
				{
					Instantiate(mission2Conver,new Vector3(0,0,0),Quaternion.identity);
					GameObject.FindWithTag("mission").GetComponent<mission_move>().guiTexture.texture = (Texture)Resources.Load("Mission(Stage9_2)");
					mission_move.MM.guiTexture.enabled = true;
					gamemanager.gm.collision_stage = false;
					Destroy(GameObject.Find("law_target"));
					Instantiate(car,new Vector3(-125,0,-875),Quaternion.identity);
					GameObject.Find("out").collider.isTrigger = true;
					GameObject.Find("arrow_3").renderer.enabled = true;
					GameObject.Find("mini_out_target").renderer.enabled = true;
					mission_2 = true;
				}
				if(gamemanager.gm._obj != null && gamemanager.gm._obj.name == "out" && gamemanager.gm.collision_stage == true && mission_2 == true)
				{
					gamemanager.gm.collision_stage = false;
					gamemanager gm = GameObject.FindWithTag("gamemanager").GetComponent<gamemanager>();
					gm.get_load_name = "scene_10";
				}
			}
		}
		
		if(Application.loadedLevelName == "scene_10")
		{
			if(gamemanager.gm._obj != null && gamemanager.gm._obj.name == "lastCar" && gamemanager.gm.collision_stage == true && missionLast == false)
			{
				gamemanager.gm.collision_stage = false;
				GameObject.FindWithTag("mission").GetComponent<mission_move>().guiTexture.texture = null;
				mission_move.MM.guiTexture.enabled = true;
				Instantiate(missionLastConver,new Vector3(0,0,0),Quaternion.identity);
				missionLast = true;
			}
			
			if(mission_move.MM.guiTexture.enabled == false && missionLast == true && one == false)
			{
				one = true;
				gamemanager.gm.collision_stage = false;
				GameObject.FindWithTag("gamemanager").GetComponent<gamemanager>().get_load_name = "main_menu";
			}
			
			if(GameObject.Find("lastStageCar") == null)
				gameover.over.end_game = true;
		}
	
	}
	
	IEnumerator handleSomething()
	{
		yield return new WaitForSeconds( 0.5f );
		GameObject.FindWithTag("gamemanager").GetComponent<gamemanager>().car_target_player = false;
	}
}
