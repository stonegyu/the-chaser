using UnityEngine;
using System.Collections;

public class sound_play : MonoBehaviour {
	public AudioClip back_music;
	public AudioClip car_engin;
	public AudioClip police;
	public AudioClip crash;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if(GameObject.FindWithTag("gamemanager").GetComponent<gamemanager>().soundOff == false){
			//transform.position = GameObject.FindWithTag("MainCamera").transform.position;
			if(GameObject.FindWithTag("gamemanager").GetComponent<gamemanager>().loading_camera_on == false){
				if(gameObject.name == "sound_BGM"){
					if(!audio.isPlaying){
							audio.clip = back_music;
							audio.Play();
							audio.volume = 0.5f;
					}
				}
				else if(gameObject.name == "sound_engin" && button.bt != null)
				{
					if(!audio.isPlaying){
						audio.loop = true;
						audio.clip = car_engin;
						audio.Play();
					}
					if(audio.isPlaying){
						audio.volume = (float)(move.mv.rigidbody.velocity.magnitude/10);
						audio.pitch = (float)(move.mv.rigidbody.velocity.magnitude/10);
					}
				}
				else if(gameObject.name == "sound_police")
				{
					if(GameObject.FindWithTag("gamemanager").GetComponent<gamemanager>().police_sound == true){
						if(!audio.isPlaying){
							audio.clip = police;
							audio.Play();
							audio.volume = 1;
						}
					}
					else
						audio.Stop();
				}
				else if(gameObject.name == "sound_crash"){
					if(!audio.isPlaying && GameObject.FindWithTag("gamemanager").GetComponent<gamemanager>().car_crash == true){
						GameObject.FindWithTag("gamemanager").GetComponent<gamemanager>().car_crash = false;
						audio.clip = crash;
						audio.Play();
						audio.volume = 1f;
					}
				}
			}
			else if(GameObject.FindWithTag("gamemanager").GetComponent<gamemanager>().loading_camera_on == true)
				audio.Stop();
		}
		else
			audio.Stop();
	}
}
