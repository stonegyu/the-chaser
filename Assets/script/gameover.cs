using UnityEngine;
using System.Collections;

public class gameover : MonoBehaviour {
	public static gameover over;
	public bool end_game = false;
	GameObject restart_rect;
	void Awake()
	{
		over = this;
	}
	// Use this for initialization
	void Start () {
		restart_rect = (GameObject)Resources.Load("restart");
	}
	
	// Update is called once per frame
	void Update () {
		if(end_game == true){
			//instance_outReck.ins_out.stop_move();
			//Time.timeScale = 0.0f;
			if(GameObject.FindWithTag("restart") == null)
				Instantiate(restart_rect,new Vector3(0,0,0),Quaternion.identity);
			if(mission_camera.MC.mission_guilayer.HitTest(Input.mousePosition) != null && mission_camera.MC.mission_guilayer.HitTest(Input.mousePosition).tag == "yes" && Input.GetMouseButton(0)){
				end_game = false;
				gamemanager gm = GameObject.FindWithTag("gamemanager").GetComponent<gamemanager>();
				gm.get_load_name = Application.loadedLevelName;
				//Time.timeScale = 1.0f;
			}
			else if(mission_camera.MC.mission_guilayer.HitTest(Input.mousePosition) != null && mission_camera.MC.mission_guilayer.HitTest(Input.mousePosition).tag == "no" && Input.GetMouseButton(0)){
				end_game = false;
				gamemanager gm = GameObject.FindWithTag("gamemanager").GetComponent<gamemanager>();
				gm.get_load_name = "main_menu";
				//Time.timeScale = 1.0f;
			}
		}
			
	
	}
}
