using UnityEngine;
using System.Collections;

public class count_time_size : MonoBehaviour {
	// Use this for initialization
	GUITexture GT;
	float time;
	void Start () {
		GT = gameObject.GetComponent<GUITexture>();
		if(gameObject.name == "number_min_10")
			GT.pixelInset = new Rect(-Screen.width*0.5f,Screen.height*0.3f,Screen.width*0.08f,Screen.width*0.08f);
		if(gameObject.name == "number_min_1")
			GT.pixelInset = new Rect(-Screen.width*0.45f,Screen.height*0.3f,Screen.width*0.08f,Screen.width*0.08f);
		if(gameObject.name == "number_sec_10")
			GT.pixelInset = new Rect(-Screen.width*0.35f,Screen.height*0.3f,Screen.width*0.08f,Screen.width*0.08f);
		if(gameObject.name == "number_sec_1")
			GT.pixelInset = new Rect(-Screen.width*0.3f,Screen.height*0.3f,Screen.width*0.08f,Screen.width*0.08f);
		if(gameObject.name == "count_dot")
			GT.pixelInset = new Rect(-Screen.width*0.4f,Screen.height*0.3f,Screen.width*0.08f,Screen.width*0.08f);
	
	}
	
	void Update()
	{
		time = time + Time.deltaTime;
		if(gameObject.name == "count_dot" && time > 2 && time < 4)
		{
			GT.enabled = false;
		}
		else if(gameObject.name == "count_dot" && time > 4)
		{
			GT.enabled = true;
			time =0;
		}
		
			
	}
}
