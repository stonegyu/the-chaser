using UnityEngine;
using System.Collections;

public class car_moveto_player : MonoBehaviour {
	float speed;
	GameObject target;
	NavMeshAgent navi;
	
	// Use this for initialization
	void Awake()
	{
		navi = gameObject.GetComponent<NavMeshAgent>();
		target = GameObject.Find("Player");
	}
	void Start () {
		if(Application.loadedLevelName == "scene_9")
			speed = 25;
		else
			speed =35f;
		navi.destination = target.transform.position;
		navi.speed = speed;
	}
	// Update is called once per frame
	void LateUpdate () {
		navi.destination = target.transform.position;
		if(GameObject.FindWithTag("gamemanager").GetComponent<gamemanager>().car_move_admit == false)
			navi.speed =0;
		else{
			navi.speed = speed;
		}
	
	}
}
