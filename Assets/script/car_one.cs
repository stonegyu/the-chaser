using UnityEngine;
using System.Collections;

public class car_one : MonoBehaviour {
	float speed;
	//bool collision = false;
	//GameObject front_car;
	GameObject target;
	NavMeshAgent navi;
	string car_num;
	public Vector3 first_position;
	bool one = false;
	float time;
	int count;
	
	// Use this for initialization
	void Awake()
	{
		first_position = gameObject.transform.position;
		car_num = gameObject.name.Split("_"[0])[1];
		navi = gameObject.GetComponent<NavMeshAgent>();
		target = GameObject.Find("one_"+car_num);
	}
	void Start () {
		speed =10;
		navi.destination = target.transform.position;
		navi.speed = speed;
	}
	// Update is called once per frame
	void LateUpdate () {
		if(one == true)
		{
			speed = 30f;
			time = time+Time.deltaTime;
		}
		else
			speed =20f;
		if(time > 5 && one == true){
			one = false;
			time =0;
		}
		navi.destination = target.transform.position;
		if(GameObject.FindWithTag("gamemanager").GetComponent<gamemanager>().car_move_admit == false)
			navi.speed =0;
		else{
			navi.speed = speed;
		}
	
	}
	
	void OnTriggerEnter(Collider other) {
		if(other.transform.gameObject.name == "one_1(Clone)" || other.transform.gameObject.name == "one_1"){
			Destroy(gameObject);
		}
		if(other.transform.gameObject.name == "Player"){
			if(count <2 && one == false){
				one = true;
				count +=1;
			}
		}
	}
}
