using UnityEngine;
using System.Collections;

public class stage_manager_2 : MonoBehaviour {
	public static stage_manager_2 stage;
	GameObject mission1Conver;
	GameObject mission2Conver;
	GameObject mission3Conver;
	GameObject explain;
	bool one = false;
	bool mission_1 = false,mission_2 = false,mission_3 = false;
	public GameObject target;
	void Awake()
	{
		stage = this;
		if(Application.loadedLevelName == "scene_2"){
			PlayerPrefs.SetInt("Scene_number",2);
			GameObject.FindWithTag("mission").GetComponent<mission_move>().guiTexture.enabled = true;
			GameObject.FindWithTag("mission").GetComponent<mission_move>().guiTexture.texture = (Texture)Resources.Load("Mission(Stage1)");
			target = GameObject.Find("police_target");
		}
		
	}
	// Use this for initialization
	void Start () {
		mission1Conver = (GameObject)Resources.Load("stage2Mission1Conver");
		mission2Conver = (GameObject)Resources.Load("stage2Mission2Conver");
		mission3Conver = (GameObject)Resources.Load("stage2Mission3Conver");
		explain = (GameObject)Resources.Load("stage2explain");
		StartCoroutine(handleSomething());
		
	}
	
	// Update is called once per frame
	void Update () {
		if(mission_move.MM != null && instance_outReck.ins_out != null && GameObject.FindWithTag("gamemanager").GetComponent<gamemanager>().loading_camera_on == false && instance_outReck.ins_out.exit_pushed == false &&
			gameover.over.end_game == false && mission_move.MM.guiTexture.enabled == false && GameObject.FindWithTag("one") == null){
			//Time.timeScale = 1.0f;
			GameObject.FindWithTag("gamemanager").GetComponent<gamemanager>().car_move_admit = true;
			pointer.PT.bPause = false;
			if(instance_outReck.ins_out != null)
				instance_outReck.ins_out.re_move();
		}
		else{
			GameObject.FindWithTag("gamemanager").GetComponent<gamemanager>().car_move_admit = false;
			//Time.timeScale =0.0f;
			if(instance_outReck.ins_out != null){
				if(GameObject.FindWithTag("stage2Explain") != null && GameObject.Find("police_3_mission_police_1") != null &&
					GameObject.Find("main_2_mission_main_1") == null){
					GameObject.FindWithTag("gamemanager").GetComponent<gamemanager>().car_move_admit = true;
				}
				else{
					instance_outReck.ins_out.stop_move();
				}
			}
		}
		
		if(Application.loadedLevelName == "scene_2"){
			if(mission_move.MM.guiTexture.enabled == false)
			{
				if(gamemanager.gm._obj != null && gamemanager.gm._obj.name == "police_target" && gamemanager.gm.collision_stage == true)
				{
					Instantiate(mission1Conver,new Vector3(0,0,0),Quaternion.identity);
					mission_move.MM.guiTexture.enabled = true;
					mission_move.MM.guiTexture.texture = (Texture)Resources.Load("Mission(Stage2)");
					gamemanager.gm.collision_stage = false;
					GameObject.FindWithTag("gamemanager").GetComponent<gamemanager>().car_target_player = true;
					Destroy(GameObject.Find("police_target"));
					GameObject.Find("arrow_2").renderer.enabled = true;
					GameObject.Find("dong_line").renderer.enabled = true;
					mission_1 = true;
					target = GameObject.Find("dong_target");
				}
				if(mission_1 == true && GameObject.FindWithTag("Panel") == null && one == false)
				{
					one = true;
					Instantiate(explain,new Vector3(0,0,0),Quaternion.identity);
					mission_move.MM.guiTexture.enabled = true;
				}
				if(gamemanager.gm._obj != null && gamemanager.gm._obj.name == "dong_target" && gamemanager.gm.collision_stage == true && mission_1 == true)
				{
					Instantiate(mission2Conver,new Vector3(0,0,0),Quaternion.identity);
					mission_move.MM.guiTexture.enabled = true;
					gamemanager.gm.collision_stage = false;
					Destroy(GameObject.Find("dong_target"));
					GameObject.Find("arrow_3").renderer.enabled = true;
					GameObject.Find("home_line").renderer.enabled = true;
					mission_2 = true;
					target = GameObject.Find("home_target");
				}
				
				if(gamemanager.gm._obj != null && gamemanager.gm._obj.name == "home_target" && gamemanager.gm.collision_stage == true && mission_2 == true)
				{
					Instantiate(mission3Conver,new Vector3(0,0,0),Quaternion.identity);
					mission_move.MM.guiTexture.enabled = true;
					gamemanager.gm.collision_stage = false;
					Destroy(GameObject.Find("home_target"));
					mission_3 = true;
					GameObject.Find("arrow_4").renderer.enabled = true;
					GameObject.Find("mini_bridge_target").renderer.enabled = true;
					GameObject.Find("target").collider.isTrigger = true;
					target = GameObject.Find("target");
				}
				if(gamemanager.gm._obj != null && gamemanager.gm._obj.name == "target" && gamemanager.gm.collision_stage == true && mission_3 == true)
				{
					gamemanager gm = GameObject.FindWithTag("gamemanager").GetComponent<gamemanager>();
					gm.get_load_name = "scene_road_2";
				}
			}
		}
		if(Application.loadedLevelName == "scene_road_2")
		{
			if(gamemanager.gm._obj != null && gamemanager.gm._obj.name == "target" && gamemanager.gm.collision_stage == true)
			{
				gamemanager.gm.collision_stage = false;
				GameObject.FindWithTag("gamemanager").GetComponent<gamemanager>().get_load_name = "scene_3";
			}
		}
	
	}
	
	IEnumerator handleSomething()
	{
		yield return new WaitForSeconds( 0.5f );
		GameObject.FindWithTag("gamemanager").GetComponent<gamemanager>().car_target_player = false;
	}
}
