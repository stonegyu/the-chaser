using UnityEngine;
using System.Collections;

public class driving_rear : MonoBehaviour {
	public static driving_rear Dr;
	public GUITexture GT;
	// Use this for initialization
	void Awake()
	{
		Dr = this;
		GT = gameObject.GetComponent<GUITexture>();
		iTween.FadeTo(gameObject,iTween.Hash("alpha",0.3f));
	}
	void Start () {
		GT.pixelInset = new Rect(Screen.width*0.35f,-Screen.height/2+Screen.height/10,Screen.width*0.12f,Screen.height*0.25f);
		
		GameObject[] objlist;
	   	objlist = GameObject.FindGameObjectsWithTag("driving_rear");
		if( objlist.Length > 1 ){
			Destroy( gameObject );
			return;
		}
	}
	
	// Update is called once per frame
	void Update () {
		if(handle.car_handle.rear == false)
			GT.texture = (Texture)Resources.Load("D");
		else
			GT.texture = (Texture)Resources.Load("R");
	
	}
}
