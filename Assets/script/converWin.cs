using UnityEngine;
using System.Collections;

public class converWin : MonoBehaviour {
	GUITexture GT;
	// Use this for initialization
	void Start () {
		GT = GetComponent<GUITexture>();
	}
	
	// Update is called once per frame
	void Update () {
		if(GameObject.FindWithTag("Panel") == null)
			GT.enabled = false;
		else
			GT.enabled = true;
	}
}
