using UnityEngine;
using System.Collections;

public class target_1 : MonoBehaviour {
	string parking;
	// Use this for initialization
	void Start () {
		parking = gameObject.name;
	}
	
	// Update is called once per frame
	void Update () {
		if(Application.loadedLevelName.Split("_"[0])[1] != "road"){
			if(GameObject.Find(parking+"_1").GetComponent<parking_target>().enter == true && 
				GameObject.Find(parking+"_2").GetComponent<parking_target>().enter == true && 
				GameObject.Find(parking+"_3").GetComponent<parking_target>().enter == true && 
				GameObject.Find(parking+"_4").GetComponent<parking_target>().enter == true)
			{
				gamemanager.gm.collision_stage = true;
				gamemanager.gm._obj = gameObject;
			}
		}
	}
}
