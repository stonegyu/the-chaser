using UnityEngine;
using System.Collections;

public class handle : MonoBehaviour {
	public static handle car_handle;
	public bool rear = false;
	Texture2D HD;
	Rect handle_rect;
	Vector3 mouse_point;
	public bool enter_mouse_1 = false;
	public bool enter_mouse_2 = false;
	float handle_rect_first_y;
	float handle_rect_end_y;
	bool bPause = false;
	// Use this for initialization
	
	void OnPause () {
		
		bPause = true;
	}
	
	void OnResume () {
		
		bPause = false;
	}
	
	void Awake()
	{
		car_handle = this;
	}
	void Start () {
		HD = (Texture2D)Resources.Load("handle");
		handle_rect = new Rect(Screen.width-Screen.width/6f,0,Screen.width/10,Screen.width/10);
		handle_rect_first_y = Screen.height/3+Screen.height/1.8f;
		handle_rect.center = new Vector2(handle_rect.center.x,handle_rect_first_y);
	}
	
	// Update is called once per frame
	void Update () {
		if(bPause == true)
			return;
		
		if(handle_rect.Contains(mouse_point) && Input.GetMouseButtonDown(0))
			enter_mouse_1 = true;
		if(enter_mouse_1 == true && !handle_rect.Contains(mouse_point))
			enter_mouse_2 = true;
		if(Input.GetMouseButtonUp(0) && (enter_mouse_1 == true || enter_mouse_2 == true))
		{
			enter_mouse_1 = false;
			enter_mouse_2 = false;
			if(handle_rect.center.y >= (handle_rect_first_y-(handle_rect_first_y-handle_rect_end_y)/2)){
				handle_rect.center = new Vector2(handle_rect.center.x,handle_rect_first_y);
				rear = false;
			}
			else{
				handle_rect.center = new Vector2(handle_rect.center.x,handle_rect_end_y);
				rear = true;
			}
		}
		if(enter_mouse_1 == true || enter_mouse_2 == true){
			handle_rect_end_y = Screen.height/4+Screen.height/2.3f;
			if(handle_rect.center.y <= handle_rect_first_y && handle_rect.center.y >= handle_rect_end_y)
				handle_rect.center = new Vector2(handle_rect.center.x,mouse_point.y);
			
			if(handle_rect.center.y >= (handle_rect_first_y-(handle_rect_first_y-handle_rect_end_y)/2))
				HD = (Texture2D)Resources.Load("handle");
			else
				HD = (Texture2D)Resources.Load("handle_reverse");
			
			if(handle_rect.center.y > handle_rect_first_y)
				handle_rect.center = new Vector2(handle_rect.center.x,handle_rect_first_y);
			else if(handle_rect.center.y <handle_rect_end_y)
				handle_rect.center = new Vector2(handle_rect.center.x,handle_rect_end_y);
		}
	}
	
	void OnGUI()
	{
		mouse_point = new Vector3(Input.mousePosition.x,Input.mousePosition.y,1);
		mouse_point.y = Screen.height-mouse_point.y;
		gamemanager gm = GameObject.FindWithTag("gamemanager").GetComponent<gamemanager>();
		if(gm.loading_camera_on == false && mission_move.MM.guiTexture.enabled == false)
			GUI.DrawTexture(handle_rect,HD);
	}
}
