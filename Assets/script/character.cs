using UnityEngine;
using System.Collections;

public class character : MonoBehaviour {

	GUITexture GT;
	public static character CT;
	public bool one = false;
	public bool chage = false;
	public string textureName;
	
	void Awake()
	{
		CT = this;
	}
	// Use this for initialization
	void Start () {
		GT = GetComponent<GUITexture>();
		gameObject.transform.position = new Vector3(-1,0.5f,1);
		iTween.FadeTo(gameObject,iTween.Hash("alpha",0f,"easetype","linear"));
	}
	
	// Update is called once per frame
	void Update () {
		if(GameObject.FindWithTag("Panel") == null){
			GT.enabled = false;
			one  =false;
			gameObject.transform.position = new Vector3(-1,0.5f,1);
			iTween.FadeTo(gameObject,iTween.Hash("alpha",0f,"easetype","linear"));
		}
		
		if((GT.enabled == true && one == false) || chage == true)
		{
			chage = false;
			one = true;
			iTween.MoveTo(gameObject,iTween.Hash("x",0.5f,"time",0.5f,"easetype","linear"));
			iTween.FadeTo(gameObject,iTween.Hash("alpha",1f,"time",0.6f,"easetype","linear"));
			GT.texture = (Texture)Resources.Load(textureName);
		}
		
	}
	
	public void transfromCharacter()
	{
		iTween.FadeTo(gameObject,iTween.Hash("alpha",0f,"easetype","linear"));
		gameObject.transform.position = new Vector3(-1,0.5f,1);
	}
}
