using UnityEngine;
using System.Collections;

public class countDown : MonoBehaviour {
	public  GUITexture[] guiTexture_count;
	public Texture2D[] Texture_count;
	public int stage_time;
	int min;
	int sec;
	float count;
	float time;
	bool bPause = false;
	void OnPause () {
		
		bPause = true;
	}
	
	void OnResume () {
		
		bPause = false;
	}
	
	// Use this for initialization
	void Awake()
	{
		
	}
	void Start () {
		stage_time =GameObject.FindWithTag("gamemanager").GetComponent<gamemanager>().stage_admitTime;
		time=0;
	}
	
	// Update is called once per frame
	void Update () {
		
		if(bPause == true)
			return;
		//gameover.over.end_game = true;
		
		time = time + Time.deltaTime;
		count = stage_time-time;
		if(count <0){
			count =0;
			gameover.over.end_game = true;
		}
		min = (int)count/60;
		sec = (int)(count - min*60);
		int one_min;
		int two_min;
		int one_sec;
		int two_sec;
		one_min = min%10;
		two_min = min/10;
		one_sec = sec%10;
		two_sec = sec/10;
		//GT.text = min_string+":"+sec.ToString()+":"+upper_sec.ToString();
		
		guiTexture_count[0].texture = Texture_count[two_min];
		guiTexture_count[1].texture = Texture_count[one_min];
		guiTexture_count[2].texture = Texture_count[two_sec];
		guiTexture_count[3].texture = Texture_count[one_sec];
	
	}
}
