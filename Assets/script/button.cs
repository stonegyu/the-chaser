using UnityEngine;
using System.Collections;

public class button : MonoBehaviour {
	public static button bt;
	public bool button_down = false;
	public float fltime;
	GUITexture GT;
	bool bPause = false;
	bool road;
	// Use this for initialization
	void Awake()
	{
		GT = gameObject.transform.GetComponent<GUITexture>();
		bt = this;
		iTween.FadeTo(gameObject,iTween.Hash("alpha",0.3f));
	}
	void Start () {
		GT.pixelInset = new Rect(-Screen.width/2.13f,-Screen.height/2,Screen.width/8,Screen.height/5);
		GameObject[] objlist;
	   	objlist = GameObject.FindGameObjectsWithTag("button");
		if( objlist.Length > 1 ){
			Destroy( gameObject );
			return;
		}
		
		if(Application.loadedLevelName.Split("_"[0])[1] == "road" || Application.loadedLevelName == "scene_4")
			road = true;
		else
			road = false;
	}
	void OnPause () {
		
		bPause = true;
	}
	
	void OnResume () {
		
		bPause = false;
	}
	
	// Update is called once per frame
	void Update () {
		if(bPause == true)
			return;
		
		if(camera_move.CM.main_GUILayer.HitTest(Input.mousePosition) != null && camera_move.CM.main_GUILayer.HitTest(Input.mousePosition).tag == "button" 
			&& Input.GetMouseButton(0) && screen_move.SM.click == false)
		{
			if(road == true){
				if(fltime <= 25)
					fltime += Time.deltaTime*3;
			}
			else
			{
				if(fltime <= 15)
					fltime += Time.deltaTime*3;
			}
			GT.texture = (Texture)Resources.Load("button_pushed");
			button_down = true;
		}
		else
		{
			button_down = false;
			GT.texture = (Texture)Resources.Load("button");
		}
	}
}