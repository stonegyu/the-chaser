using UnityEngine;
using System.Collections;

public class back_mirror : MonoBehaviour {
	public static back_mirror BM;
	GUITexture GT;
	GameObject cb;
	public Camera came;
	public Camera main_came;
	public Vector3 temp;
	public Vector3 temp_front;
	public bool back_touched = false;
	public bool touch_noting = false;
	bool bPause = false;
	// Use this for initialization
	
	void OnPause () {
		
		bPause = true;
	}
	
	void OnResume () {
		
		bPause = false;
	}
	
	void Awake()
	{
		BM = this;
		GT = transform.GetComponent<GUITexture>();
		cb = GameObject.FindWithTag("cube");
		GT.pixelInset = new Rect(-Screen.width*0.17f,Screen.height*0.27f,Screen.width*0.345f,Screen.height*0.27f);
	}
	void Start () {
		
		GameObject[] objlist;
	   	objlist = GameObject.FindGameObjectsWithTag("emty");
		if( objlist.Length > 1 ){
			Destroy( gameObject );
			return;
		}
	}
	
	// Update is called once per frame
	void Update () {
		if(bPause == true)
			return;
		
		if(back_touched == false)
			GT.pixelInset = new Rect(-Screen.width*0.17f,Screen.height*0.27f,Screen.width*0.345f,Screen.height*0.27f);
		if(camera_move.CM.main_GUILayer.HitTest(Input.mousePosition) == null)
			touch_noting = true;
		else
			touch_noting = false;
		if(camera_move.CM.main_GUILayer.HitTest(Input.mousePosition) != null && camera_move.CM.main_GUILayer.HitTest(Input.mousePosition).tag =="emty" &&
			Input.GetMouseButtonDown(0) && screen_move.SM.end_itween == false)
		{
			back_touched =true;
			main_came.transform.localPosition = temp;
			main_came.transform.localPosition = came.transform.localPosition;
			main_came.transform.eulerAngles = new Vector3(main_came.transform.eulerAngles.x,cb.transform.localEulerAngles.y+180,main_came.transform.eulerAngles.z);
			came.rect = new Rect(0.652f,0.495f,0.3f,0.21f);
			GT.pixelInset = new Rect(Screen.width*0.13f,-Screen.height*0.035f,Screen.width*0.345f,Screen.height*0.27f);
			
		}
		
		if(back_touched == true && camera_move.CM.main_GUILayer.HitTest(Input.mousePosition) != null && camera_move.CM.main_GUILayer.HitTest(Input.mousePosition).tag =="emty" &&
			Input.GetMouseButtonDown(0))
		{
			back_touched = false;
			came.rect = new Rect(0.35f,0.8f,0.3f,0.2f);
			main_came.transform.localPosition = temp_front;
			main_came.transform.localPosition = came.transform.localPosition;
			main_came.transform.eulerAngles = new Vector3(main_came.transform.eulerAngles.x,cb.transform.localEulerAngles.y,main_came.transform.eulerAngles.z);
		}
	
	}
}
