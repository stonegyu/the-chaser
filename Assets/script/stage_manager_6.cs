using UnityEngine;
using System.Collections;

public class stage_manager_6 : MonoBehaviour {

	public static stage_manager_6 stage;
	GameObject mission2Conver;
	GameObject mission3Conver;
	GameObject mission4Conver;
	bool mission_1 = false,mission_2 = false;
	int rand_num;
	void Awake()
	{
		stage = this;
		if(Application.loadedLevelName == "scene_6"){
			PlayerPrefs.SetInt("Scene_number",6);
			GameObject.FindWithTag("mission").GetComponent<mission_move>().guiTexture.enabled = true;
			GameObject.FindWithTag("mission").GetComponent<mission_move>().guiTexture.texture = (Texture)Resources.Load("Mission(Stage1)");
		}
		
	}
	// Use this for initialization
	void Start () {
		mission2Conver = (GameObject)Resources.Load("stage6Mission2Conver");
		mission3Conver = (GameObject)Resources.Load("stage6Mission3Conver");
		mission4Conver = (GameObject)Resources.Load("stage6Mission4Conver");
		StartCoroutine(handleSomething());
		rand_num = Random.Range(1,9);
		mission_move.MM.guiTexture.texture = (Texture)Resources.Load("Mission(Stage1)");
		mission_move.MM.guiTexture.enabled = true;
	}
	
	// Update is called once per frame
	void Update () {
		if(mission_move.MM != null && instance_outReck.ins_out != null && GameObject.FindWithTag("gamemanager").GetComponent<gamemanager>().loading_camera_on == false && instance_outReck.ins_out.exit_pushed == false &&
			gameover.over.end_game == false && mission_move.MM.guiTexture.enabled == false && GameObject.FindWithTag("one") == null){
			//Time.timeScale = 1.0f;
			GameObject.FindWithTag("gamemanager").GetComponent<gamemanager>().car_move_admit = true;
			pointer.PT.bPause = false;
			if(instance_outReck.ins_out != null)
				instance_outReck.ins_out.re_move();
		}
		else{
			GameObject.FindWithTag("gamemanager").GetComponent<gamemanager>().car_move_admit = false;
			//Time.timeScale =0.0f;
			if(instance_outReck.ins_out != null)
				instance_outReck.ins_out.stop_move();
		}
		
		if(Application.loadedLevelName == "scene_6"){
			if(mission_move.MM.guiTexture.enabled == false)
			{	
				if(gamemanager.gm._obj != null && gamemanager.gm._obj.name == "gasStation_target" && gamemanager.gm.collision_stage == true)
				{
					Instantiate(mission2Conver,new Vector3(0,0,0),Quaternion.identity);
					mission_move.MM.guiTexture.enabled = true;
					gamemanager.gm.collision_stage = false;
					Destroy(GameObject.Find("gasStation_target"));
					mission_1 = true;
					GameObject.Find("arrow_1").renderer.enabled = true;
					GameObject.Find("hospital1_line").renderer.enabled = true;
					GameObject.Find("arrow_2").renderer.enabled = true;
					GameObject.Find("hospital2_line").renderer.enabled = true;
					GameObject.Find("arrow_3").renderer.enabled = true;
					GameObject.Find("hospital3_line").renderer.enabled = true;
					GameObject.Find("arrow_4").renderer.enabled = true;
					GameObject.Find("hospital4_line").renderer.enabled = true;
					GameObject.Find("arrow_5").renderer.enabled = true;
					GameObject.Find("hospital5_line").renderer.enabled = true;
					GameObject.Find("arrow_6").renderer.enabled = true;
					GameObject.Find("hospital6_line").renderer.enabled = true;
					GameObject.Find("arrow_7").renderer.enabled = true;
					GameObject.Find("hospital7_line").renderer.enabled = true;
					GameObject.Find("arrow_8").renderer.enabled = true;
					GameObject.Find("hospital8_line").renderer.enabled = true;
				}
				if(gamemanager.gm._obj != null && gamemanager.gm._obj.name == "hospital"+rand_num+"_target" && gamemanager.gm.collision_stage == true && mission_1 == true)
				{
					Instantiate(mission4Conver,new Vector3(0,0,0),Quaternion.identity);
					mission_move.MM.guiTexture.texture = (Texture)Resources.Load("Mission(Stage6)");
					mission_move.MM.guiTexture.enabled = true;
					gamemanager.gm.collision_stage = false;
					Destroy(GameObject.Find("hospital1_target"));
					Destroy(GameObject.Find("hospital2_target"));
					Destroy(GameObject.Find("hospital3_target"));
					Destroy(GameObject.Find("hospital4_target"));
					Destroy(GameObject.Find("hospital5_target"));
					Destroy(GameObject.Find("hospital6_target"));
					Destroy(GameObject.Find("hospital7_target"));
					Destroy(GameObject.Find("hospital8_target"));
					mission_2 = true;
					GameObject.Find("arrow_10").renderer.enabled = true;
					GameObject.Find("mini_out_target").renderer.enabled = true;
					GameObject.Find("out_gard").collider.isTrigger = true;
				}
				else if(gamemanager.gm._obj != null && gamemanager.gm._obj.name.Split("_"[0])[1] == "target" && gamemanager.gm.collision_stage == true && mission_1 == true)
				{
					Instantiate(mission3Conver,new Vector3(0,0,0),Quaternion.identity);
					mission_move.MM.guiTexture.enabled = true;
					gamemanager.gm.collision_stage = false;
					Destroy(GameObject.Find(gamemanager.gm._obj.name));
				}
				if(gamemanager.gm._obj != null && gamemanager.gm._obj.name == "out_gard" && gamemanager.gm.collision_stage == true && mission_2 == true)
				{
					gamemanager.gm.collision_stage = false;
					gamemanager gm = GameObject.FindWithTag("gamemanager").GetComponent<gamemanager>();
					gm.get_load_name = "scene_road_6";
				}
			}
		}
		if(Application.loadedLevelName == "scene_road_6"){
			if(gamemanager.gm._obj != null && gamemanager.gm._obj.name == "target" && gamemanager.gm.collision_stage == true)
			{
				gamemanager.gm.collision_stage = false;
				GameObject.FindWithTag("gamemanager").GetComponent<gamemanager>().get_load_name = "scene_7";
			}
		}
	
	}
	
	IEnumerator handleSomething()
	{
		yield return new WaitForSeconds( 0.5f );
		GameObject.FindWithTag("gamemanager").GetComponent<gamemanager>().car_target_player = false;
	}
}
