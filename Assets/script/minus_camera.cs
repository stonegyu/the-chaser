using UnityEngine;
using System.Collections;

public class minus_camera : MonoBehaviour {
	public static minus_camera minus_CM;
	public Camera minus_camera_script;
	GameObject cb;
	// Use this for initialization
	void Awake()
	{
		minus_camera_script = transform.GetComponent<Camera>();
		minus_CM = this;
		cb = GameObject.FindWithTag("cube");
	}
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		move_minus_camera();//back mirror touched minus camera move to main camera position and rotate angle
		if(screen_move.SM.click == false && screen_move.SM.end_itween == false && back_mirror.BM.back_touched == true)
			gameObject.transform.eulerAngles = move.mv.transform.eulerAngles;
		if(screen_move.SM.click == false && screen_move.SM.end_itween == false && back_mirror.BM.back_touched == false)
			gameObject.transform.eulerAngles = new Vector3(move.mv.transform.eulerAngles.x,move.mv.transform.eulerAngles.y-180,move.mv.transform.eulerAngles.z);
	}
	
	void move_minus_camera()
	{
		if(back_mirror.BM.back_touched == true){
			transform.localPosition = back_mirror.BM.temp;
			transform.eulerAngles = new Vector3(transform.eulerAngles.x,cb.transform.eulerAngles.y,transform.eulerAngles.z);
		}
		else if(back_mirror.BM.back_touched == false)
		{
			transform.localPosition = back_mirror.BM.temp_front;
			transform.eulerAngles = new Vector3(transform.eulerAngles.x,cb.transform.eulerAngles.y+180,transform.eulerAngles.z);
		}
	}
}
