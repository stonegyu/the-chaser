using UnityEngine;
using System.Collections;

public class stage_manager_5 : MonoBehaviour {
	public static stage_manager_5 stage;
	GameObject mission1Conver;
	GameObject mission2Conver;
	bool mission_1 = false;
	int rand_num;
	void Awake()
	{
		stage = this;
		if(Application.loadedLevelName == "scene_5"){
			PlayerPrefs.SetInt("Scene_number",5);
			GameObject.FindWithTag("mission").GetComponent<mission_move>().guiTexture.enabled = true;
			GameObject.FindWithTag("mission").GetComponent<mission_move>().guiTexture.texture = (Texture)Resources.Load("Mission(Stage1)");
		}
		if(Application.loadedLevelName == "scene_road_5"){
			PlayerPrefs.SetInt("Scene_number",5);
		}
		
	}
	// Use this for initialization
	void Start () {
		mission1Conver = (GameObject)Resources.Load("stage5Mission1Conver");
		mission2Conver = (GameObject)Resources.Load("stage5Mission2Conver");
		StartCoroutine(handleSomething());
		rand_num = Random.Range(1,4);
	}
	
	// Update is called once per frame
	void Update () {
		if(mission_move.MM != null && instance_outReck.ins_out != null && GameObject.FindWithTag("gamemanager").GetComponent<gamemanager>().loading_camera_on == false && instance_outReck.ins_out.exit_pushed == false &&
			gameover.over.end_game == false && mission_move.MM.guiTexture.enabled == false && GameObject.FindWithTag("one") == null){
			//Time.timeScale = 1.0f;
			GameObject.FindWithTag("gamemanager").GetComponent<gamemanager>().car_move_admit = true;
			pointer.PT.bPause = false;
			if(instance_outReck.ins_out != null)
				instance_outReck.ins_out.re_move();
		}
		else{
			GameObject.FindWithTag("gamemanager").GetComponent<gamemanager>().car_move_admit = false;
			//Time.timeScale =0.0f;
			if(instance_outReck.ins_out != null)
				instance_outReck.ins_out.stop_move();
		}
		
		if(Application.loadedLevelName == "scene_5"){
			if(mission_move.MM.guiTexture.enabled == false)
			{	
				if(gamemanager.gm._obj != null && gamemanager.gm._obj.name == "hospital"+rand_num+"_target" && gamemanager.gm.collision_stage == true && mission_1 == false)
				{
					Instantiate(mission2Conver,new Vector3(0,0,0),Quaternion.identity);
					mission_move.MM.guiTexture.texture = (Texture)Resources.Load("Mission(Stage5)");
					mission_move.MM.guiTexture.enabled = true;
					gamemanager.gm.collision_stage = false;
					mission_1 = true;
					GameObject.Find("out_gard").collider.isTrigger = true;
					GameObject.Find("arrow_4").renderer.enabled = true;
					GameObject.Find("mini_out_target").renderer.enabled = true;
					Destroy(GameObject.Find("hospital1_target"));
					Destroy(GameObject.Find("hospital2_target"));
					Destroy(GameObject.Find("hospital3_target"));
				}
				else if(gamemanager.gm._obj != null && gamemanager.gm._obj.name.Split("_"[0])[1] == "target" && gamemanager.gm.collision_stage == true)
				{
					Instantiate(mission1Conver,new Vector3(0,0,0),Quaternion.identity);
					mission_move.MM.guiTexture.enabled = true;
					gamemanager.gm.collision_stage = false;
					Destroy(GameObject.Find(gamemanager.gm._obj.name));
				}
				if(gamemanager.gm._obj != null && gamemanager.gm._obj.name == "out_gard" && gamemanager.gm.collision_stage == true && mission_1 == true)
				{
					gamemanager.gm.collision_stage = false;
					gamemanager gm = GameObject.FindWithTag("gamemanager").GetComponent<gamemanager>();
					gm.get_load_name = "scene_road_5";
				}
			}
		}
		if(Application.loadedLevelName == "scene_road_5"){
			if(gamemanager.gm._obj != null && gamemanager.gm._obj.name == "target" && gamemanager.gm.collision_stage == true)
			{
				gamemanager.gm.collision_stage = false;
				GameObject.FindWithTag("gamemanager").GetComponent<gamemanager>().get_load_name = "scene_6";
			}
		}
	
	}
	
	IEnumerator handleSomething()
	{
		yield return new WaitForSeconds( 0.5f );
		GameObject.FindWithTag("gamemanager").GetComponent<gamemanager>().car_target_player = false;
	}
}
