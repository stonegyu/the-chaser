using UnityEngine;
using System.Collections;

public class sky_move : MonoBehaviour {
	float offset;
	bool reverse = false;
	
	void Awake()
	{
	}
	//bool bPause = false;
	// Use this for initialization
	void Start () {
		
		GameObject[] objlist;
	   	objlist = GameObject.FindGameObjectsWithTag("skybox");
		if( objlist.Length > 1 ){
			Destroy( gameObject );
			return;
		}
	
	}
	/*
	void OnPause () {
		
		bPause = true;
	}
	
	void OnResume () {
		
		bPause = false;
	}*/
	// Update is called once per frame
	void Update () {
		//if(bPause == true)
		//	return;
		
		if(reverse == false){
			offset += Time.deltaTime*0.01f;
			if(offset > 0.2f)
				reverse = true;
		}
		else if(reverse == true){
			offset -= Time.deltaTime*0.01f;
				if(offset < -0.2f)
					reverse = false;
		}
		renderer.material.mainTextureOffset = new Vector2(offset,offset);
	
	}
}
