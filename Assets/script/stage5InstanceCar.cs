using UnityEngine;
using System.Collections;

public class stage5InstanceCar : MonoBehaviour {
	GameObject car;
	string car_num;
	float fltime;
	// Use this for initialization
	void Awake()
	{
		car_num = gameObject.name.Split("_"[0])[1];
		car = (GameObject)Resources.Load("stage5car_"+car_num);
	}
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if(mission_move.MM.guiTexture.enabled == false){
			fltime += Time.deltaTime;
			if(fltime >2){
				Instantiate(car,gameObject.transform.position,Quaternion.identity);
				fltime =0;
			}
		}
	
	}
}
