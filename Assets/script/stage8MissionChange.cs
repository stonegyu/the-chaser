using UnityEngine;
using System.Collections;

public class stage8MissionChange : MonoBehaviour {
	bool one = false;
	// Use this for initialization
	void Start () {
		GameObject.FindWithTag("mission").guiTexture.texture = (Texture)Resources.Load("Mission(Stage8)");
	
	}
	
	// Update is called once per frame
	void Update () {
		if(GameObject.Find("naration_2_story_null_1") == null && GameObject.Find("naration_4_story_null_1") != null)
			GameObject.FindWithTag("mission").guiTexture.texture = (Texture)Resources.Load("Mission(Stage8_1)");
		else if(GameObject.Find("naration_4_story_null_1") == null && one == false){
			GameObject.FindWithTag("mission").guiTexture.texture = (Texture)Resources.Load("Mission(Stage8_2)");
			one = true;
		}
	
	}
}
