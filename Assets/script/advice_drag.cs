using UnityEngine;
using System.Collections;

public class advice_drag : MonoBehaviour {
	GameObject up;
	GUITexture GT;
	public static advice_drag AD;
	// Use this for initialization
	void Awake()
	{
		AD = this;
		
		GT = gameObject.GetComponent<GUITexture>();
	}
	void Start () {
		GT.pixelInset = new Rect(-Screen.width/4f,-Screen.height/2f,Screen.width/2f,Screen.width/4f);
		iTween.FadeTo(gameObject,iTween.Hash("alpha",0f,"time",1f,"loopType","pingPong"));
		up = (GameObject)Resources.Load("up_drag_fab");
		Instantiate(up,new Vector3(0.55f,0.3f,2),Quaternion.identity);
	}
}
