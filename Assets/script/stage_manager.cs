using UnityEngine;
using System.Collections;

public class stage_manager : MonoBehaviour {
	public static stage_manager stage;
	GameObject mission2Conver;
	GameObject mission3Conver;
	GameObject mission4Conver;
	GameObject advice_accel;
	GameObject advice_accel_2;
	GameObject advice_brake;
	GameObject advice_back_mirro;
	GameObject advice_back_mirro_2;
	GameObject advice_D_R;
	GameObject advice_see;
	GameObject advice_time;
	GameObject left;
	GameObject right;
	GameObject up;
	public GameObject target;
//	GameObject drag;
	bool advice_1= false,advice_2= false,advice_3= false,advice_4= false,advice_5= false,advice_6= false,advice_7= false,advice_8= false;
	int count;
	bool mission_1 = false,mission_2 = false,mission_3 = false;
	// Use this for initialization
	void Awake()
	{
		stage = this;
		if(Application.loadedLevelName == "scene_1"){
			PlayerPrefs.SetInt("Scene_number",1);
			mission_move.MM.guiTexture.texture = (Texture)Resources.Load("Mission(Stage1)");
			target = GameObject.Find("clothes_target");
		}
		
	}
	void Start () {
		mission2Conver = (GameObject)Resources.Load("stage1Mission2Conver");
		mission3Conver = (GameObject)Resources.Load("stage1Mission3Conver");
		mission4Conver = (GameObject)Resources.Load("stage1Mission4Conver");
		advice_accel = (GameObject)Resources.Load("Advice_accel_fab");
		advice_accel_2 = (GameObject)Resources.Load("Advice_accel_2_fab");
		advice_brake = (GameObject)Resources.Load("Advice_brake_fab");
		advice_D_R = (GameObject)Resources.Load("Advice_D_R_fab");
		advice_back_mirro = (GameObject)Resources.Load("Advice_back_mirro_fab");
		advice_back_mirro_2 = (GameObject)Resources.Load("Advice_back_mirro_2_fab");
		advice_see = (GameObject)Resources.Load("Advice_see_fab");
		advice_time = (GameObject)Resources.Load("Advice_time_fab");
		left = (GameObject)Resources.Load("left_fab");
		right = (GameObject)Resources.Load("right_fab");
		up = (GameObject)Resources.Load("up_fab");
//		drag = (GameObject)Resources.Load("drag_fab");
		
		advice_1 = false;
		advice_2 = false;
		advice_3 = false;
		advice_4 = false;
		advice_5 = false;
		advice_6 = false;
		advice_7 = false;
		advice_8= false;
		count =0;
		
		StartCoroutine(handleSomething());
		
		mission_move.MM.guiTexture.enabled = true;
	}
	// Update is called once per frame
	void Update () {
		if(mission_move.MM != null && instance_outReck.ins_out != null && GameObject.FindWithTag("gamemanager").GetComponent<gamemanager>().loading_camera_on == false && instance_outReck.ins_out.exit_pushed == false &&
			gameover.over.end_game == false && mission_move.MM.guiTexture.enabled == false && GameObject.FindWithTag("one") == null){
			//Time.timeScale = 1.0f;
			GameObject.FindWithTag("gamemanager").GetComponent<gamemanager>().car_move_admit = true;
			pointer.PT.bPause = false;
			if(instance_outReck.ins_out != null)
				instance_outReck.ins_out.re_move();
		}
		else{
			GameObject.FindWithTag("gamemanager").GetComponent<gamemanager>().car_move_admit = false;
			//Time.timeScale =0.0f;
			if(instance_outReck.ins_out != null)
				stop_move();
		}
		
		if(Application.loadedLevelName == "scene_1"){
			if(mission_move.MM.guiTexture.enabled == false)
			{
				if(advice_1 == false){
					if(GameObject.FindWithTag("one") == null){
						Instantiate(advice_accel,new Vector3(0.5f,0.35f,1),Quaternion.identity);
						Instantiate(left,new Vector3(0.35f,0.1f,0),Quaternion.identity);
					}
					if(button.bt.button_down == true && GameObject.FindWithTag("one") != null)
					{
						Destroy(GameObject.FindWithTag("two"));
						Destroy(GameObject.FindWithTag("one"));
						advice_1 = true;
					}
				}
				if(advice_1 == true && advice_2 == false)
				{
					count++;
					if(GameObject.FindWithTag("one") == null){
						Instantiate(advice_accel_2,new Vector3(0.5f,0.35f,1),Quaternion.identity);
					}
					if(count > 150 && GameObject.FindWithTag("one") != null)
					{
						Destroy(GameObject.FindWithTag("one"));
						advice_2 = true;
						count =0;
					}
				}
				if(gamemanager.gm._obj != null && gamemanager.gm._obj.name == "mission_out" && gamemanager.gm.collision_stage == true){
					if(advice_3 == false){
						if(GameObject.FindWithTag("one") == null){
							Instantiate(advice_brake,new Vector3(0.5f,0.35f,1),Quaternion.identity);
							Instantiate(left,new Vector3(0.35f,0.35f,0),Quaternion.identity);
						}
						if(brake.bk.brake_douwn == true && GameObject.FindWithTag("one") != null)
						{
							Destroy(GameObject.FindWithTag("two"));
							Destroy(GameObject.FindWithTag("one"));
							advice_3 = true;
							gamemanager.gm.collision_stage = false;
						}
					}
				}
			}
			
			
			if(gamemanager.gm._obj != null && gamemanager.gm._obj.name == "clothes_target" && gamemanager.gm.collision_stage == true)
			{
				if(GameObject.FindWithTag("two") != null && advice_3 == false)
				{
					Destroy(GameObject.FindWithTag("two"));
					advice_3 = true;
				}
				Instantiate(mission2Conver,new Vector3(0,0,0),Quaternion.identity);
				mission_move.MM.guiTexture.texture = null;
				mission_move.MM.guiTexture.enabled = true;
				gamemanager.gm.collision_stage = false;
				Destroy(GameObject.Find("clothes_target"));
				GameObject.Find("arrow_2").renderer.enabled = true;
				GameObject.Find("hospital_line").renderer.enabled = true;
				mission_1 = true;
				target = GameObject.Find("hospital_target");
			}
			if(mission_1 == true && mission_move.MM.guiTexture.enabled == false)
			{
				if(advice_4 == false && advice_3 == true){
					if(GameObject.FindWithTag("one") == null){
						Instantiate(advice_D_R,new Vector3(0.5f,0.35f,1),Quaternion.identity);
						Instantiate(right,new Vector3(0.85f,0.25f,0),Quaternion.identity);
					}
					if(handle.car_handle.rear == true && GameObject.FindWithTag("one") != null)
					{
						Destroy(GameObject.FindWithTag("two"));
						Destroy(GameObject.FindWithTag("one"));
						advice_4 = true;
					}
				}
				if(advice_5 == false && advice_4 == true){
					if(GameObject.FindWithTag("one") == null){
						Instantiate(advice_back_mirro,new Vector3(0.5f,0.35f,1),Quaternion.identity);
						Instantiate(up,new Vector3(0.6f,0.7f,0),Quaternion.identity);
					}
					if(back_mirror.BM.back_touched == true && GameObject.FindWithTag("one") != null)
					{
						Destroy(GameObject.FindWithTag("one"));
						Destroy(GameObject.FindWithTag("two"));
						advice_5 = true;
					}
				}
				if(gamemanager.gm._obj != null && gamemanager.gm._obj.name == "mission1_out" && gamemanager.gm.collision_stage == true){
					if(GameObject.FindWithTag("two") != null && advice_5 == false){
						Destroy(GameObject.FindWithTag("two"));
						advice_5 = true;
					}
					if(advice_6 == false && advice_5 == true){
						if(GameObject.FindWithTag("one") == null){
							Instantiate(advice_back_mirro_2,new Vector3(0.5f,0.35f,1),Quaternion.identity);
							Instantiate(right,new Vector3(0.5f,0.6f,0),Quaternion.identity);
						}
						if(back_mirror.BM.back_touched == false && GameObject.FindWithTag("one") != null)
						{
							Destroy(GameObject.FindWithTag("one"));
							Destroy(GameObject.FindWithTag("two"));
							advice_6 = true;
						}
					}
					if(advice_7 == false && advice_6 == true){
						if(GameObject.FindWithTag("one") == null){
							Instantiate(advice_see,new Vector3(0.5f,0.35f,1),Quaternion.identity);
							//Instantiate(drag,new Vector3(0.55f,0.75f,0),Quaternion.identity);
						}
						if(screen_move.SM.end_itween == true && GameObject.FindWithTag("one") != null)
						{
							Destroy(GameObject.FindWithTag("one"));
							Destroy(GameObject.FindWithTag("two"));
							advice_7 = true;
						}
					}
					if(advice_8 == false && advice_7 == true){
						count++;
						if(GameObject.FindWithTag("one") == null && advice_8 == false){
							Instantiate(advice_time,new Vector3(0.5f,0.35f,1),Quaternion.identity);
						}
						if(count > 150 && GameObject.FindWithTag("one") != null)
						{
							count =0;
							Destroy(GameObject.FindWithTag("one"));
							advice_8 = true; 
							gamemanager.gm.collision_stage = false;
						}
					}
				}
			}
			if(gamemanager.gm._obj != null && gamemanager.gm._obj.name == "hospital_target" && gamemanager.gm.collision_stage == true && mission_1 == true)
			{
				Instantiate(mission3Conver,new Vector3(0,0,0),Quaternion.identity);
				mission_move.MM.guiTexture.texture = (Texture)Resources.Load("Mission(Stage1_2)");
				mission_move.MM.guiTexture.enabled = true;
				gamemanager.gm.collision_stage = false;
				Destroy(GameObject.Find("hospital_target"));
				GameObject.Find("arrow_4").renderer.enabled = true;
				GameObject.Find("gasStation_line").renderer.enabled = true;
				mission_2 = true;
				target = GameObject.Find("gasStation_target2");
			}
			if(gamemanager.gm._obj != null && (gamemanager.gm._obj.name == "gasStation_target2") && gamemanager.gm.collision_stage == true && mission_2 == true)
			{
				Instantiate(mission4Conver,new Vector3(0,0,0),Quaternion.identity);
				mission_move.MM.guiTexture.texture = null;
				mission_move.MM.guiTexture.enabled = true;
				gamemanager.gm.collision_stage = false;
				Destroy(GameObject.Find("gasStation_target2"));
				GameObject.Find("arrow_5").renderer.enabled = true;
				GameObject.Find("mini_gotoBridge_target").renderer.enabled = true;
				mission_3 = true;
				GameObject.Find("goto_bridge").collider.isTrigger = true;
				target = GameObject.Find("goto_bridge");
			}
			if(gamemanager.gm._obj != null && gamemanager.gm._obj.name == "goto_bridge" && gamemanager.gm.collision_stage == true && mission_3 == true)
			{
				gamemanager gm = GameObject.FindWithTag("gamemanager").GetComponent<gamemanager>();
				gm.get_load_name = "scene_road_1";
			}
			
		}
		
		if(Application.loadedLevelName == "scene_road_1")
		{
			if(gamemanager.gm._obj != null && gamemanager.gm._obj.name == "target" && gamemanager.gm.collision_stage == true)
			{
				gamemanager.gm.collision_stage = false;
				GameObject.FindWithTag("gamemanager").GetComponent<gamemanager>().get_load_name = "scene_2";
			}
		}
		
	
	}
	/*
	GameObject get_target(GameObject target_1,GameObject target_2)
	{
		Vector2 target_origin;
		float length_1,length_2;
		target_origin = move.mv.transform.position;
		
		length_1 = Mathf.Sqrt((target_origin.x - target_1.transform.position.x)*(target_origin.x - target_1.transform.position.x) + (target_origin.y - target_1.transform.position.z)*(target_origin.y - target_1.transform.position.z));
		length_2 = Mathf.Sqrt((target_origin.x - target_2.transform.position.x)*(target_origin.x - target_2.transform.position.x) + (target_origin.y - target_2.transform.position.z)*(target_origin.y - target_2.transform.position.z));
		
		if(length_1 > length_2)
			return target_2;
		else
			return target_2;
	}*/
	
	IEnumerator handleSomething()
	{
		yield return new WaitForSeconds( 0.5f );
		GameObject.FindWithTag("gamemanager").GetComponent<gamemanager>().car_target_player = false;
	}
	
	void stop_move()
	{
		GameObject[] gameobjects = FindObjectsOfType(typeof(GameObject)) as GameObject[];        
		foreach (GameObject gameObj in gameobjects) {  
			if(advice_1 == false && GameObject.FindWithTag("one") != null)
			{
				if(gameObj.name == "button" || gameObj.name == "pointer" || gameObj.name == "handle"
				|| gameObj.name == "time" || gameObj.name == "arrow" || gameObj.name == "Screen_move"
				|| gameObj.name == "emty" || gameObj.name == "Player")
				gameObj.SendMessage( "OnPause", 1f );
			}
			if(advice_2 == false && GameObject.FindWithTag("one") != null)
			{
				if( gameObj.name == "button" || gameObj.name == "pointer" || gameObj.name == "handle"
					|| gameObj.name == "time" || gameObj.name == "arrow" || gameObj.name == "Screen_move"
					|| gameObj.name == "emty")
					gameObj.SendMessage( "OnPause", 1f );  
			}
			else if(advice_3 == false && GameObject.FindWithTag("one") != null)
			{
				if( gameObj.name == "brake" || gameObj.name == "pointer" || gameObj.name == "handle"
					|| gameObj.name == "time" || gameObj.name == "arrow" || gameObj.name == "Screen_move"
					|| gameObj.name == "emty" || gameObj.name == "Player")
					gameObj.SendMessage( "OnPause", 1f ); 
			}
			else if(advice_4 == false && GameObject.FindWithTag("one") != null)
			{
				if( gameObj.name == "button" || gameObj.name == "brake" || gameObj.name == "pointer"
					|| gameObj.name == "time" || gameObj.name == "arrow" || gameObj.name == "Screen_move"
					|| gameObj.name == "emty" || gameObj.name == "Player")
					gameObj.SendMessage( "OnPause", 1f );   
			}
			else if(advice_5 == false && GameObject.FindWithTag("one") != null)
			{
				if( gameObj.name == "button" || gameObj.name == "brake" || gameObj.name == "pointer" || gameObj.name == "handle"
					|| gameObj.name == "time" || gameObj.name == "arrow" || gameObj.name == "Screen_move"
					|| gameObj.name == "Player")
					gameObj.SendMessage( "OnPause", 1f );   
			}
			else if(advice_6 == false && GameObject.FindWithTag("one") != null)
			{
				if( gameObj.name == "button" || gameObj.name == "brake" || gameObj.name == "pointer" || gameObj.name == "handle"
					|| gameObj.name == "time" || gameObj.name == "arrow" || gameObj.name == "Screen_move"
					|| gameObj.name == "Player")
					gameObj.SendMessage( "OnPause", 1f );   
			}
			else if(advice_7 == false && GameObject.FindWithTag("one") != null)
			{
				if( gameObj.name == "button" || gameObj.name == "brake" || gameObj.name == "pointer" || gameObj.name == "handle"
					|| gameObj.name == "time" || gameObj.name == "arrow" || gameObj.name == "emty" || gameObj.name == "Player")
					gameObj.SendMessage( "OnPause", 1f );
			}
			else{
				if( gameObj.name == "button" || gameObj.name == "brake" || gameObj.name == "pointer" || gameObj.name == "handle"
					|| gameObj.name == "time" || gameObj.name == "arrow" || gameObj.name == "Screen_move"
					|| gameObj.name == "emty" || gameObj.name == "Player")
					gameObj.SendMessage( "OnPause", 1f );   
			}
		}
	}
}
