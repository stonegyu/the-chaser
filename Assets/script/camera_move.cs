using UnityEngine;
using System.Collections;

public class camera_move : MonoBehaviour {
	public static camera_move CM;
	public GUILayer main_GUILayer;
	Vector3 target;
	Vector3 original_position;
	GameObject cb;
	Vector3 back_position;
	Vector3 brake_target,brake_original;
	float cb_vel;
	// Use this for initialization
	void Awake()
	{
		CM = this;
		main_GUILayer = Camera.mainCamera.GetComponent<GUILayer>();
		cb = GameObject.FindWithTag("cube");
	}
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		main_move_front_back();//if you push button main camera move back, or you push brake camera move front
		if(screen_move.SM.click == false && screen_move.SM.end_itween == false && back_mirror.BM.back_touched == false)
			gameObject.transform.eulerAngles = move.mv.transform.eulerAngles;
		if(screen_move.SM.click == false && screen_move.SM.end_itween == false && back_mirror.BM.back_touched == true)
			gameObject.transform.eulerAngles = new Vector3(move.mv.transform.eulerAngles.x,move.mv.transform.eulerAngles.y-180,move.mv.transform.eulerAngles.z);
	}
	
	void main_move_front_back()
	{
		back_position = transform.forward;
		if(button.bt.button_down == true)
		{
			if(cb.rigidbody.velocity.sqrMagnitude <1){
				target = (cb.transform.position + transform.forward/2) - back_position/2;
				transform.position = Vector3.Lerp(transform.position,target,Time.deltaTime*10);
			}
		}
		else if(button.bt.button_down == false)
		{
			original_position = (cb.transform.position + transform.forward/2);
			transform.position = Vector3.Lerp(transform.position,original_position,Time.deltaTime*5);
		}
		if(brake.bk.brake_douwn == true && move.mv.vel_zero == false && cb.rigidbody.velocity != new Vector3(0,0,0))
		{
			brake_target = (cb.transform.position + transform.forward/2) + back_position;
			transform.position = Vector3.Lerp(transform.position,brake_target,Time.deltaTime*5);
		}
		if(brake.bk.brake_douwn == false || move.mv.vel_zero == true)
		{
			brake_original = (cb.transform.position + transform.forward/2);
			transform.position = Vector3.Lerp(transform.position,brake_original,Time.deltaTime*5);
		}
	}
}
