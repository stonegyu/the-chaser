using UnityEngine;
using System.Collections;

public class stage6CarMove : MonoBehaviour {

	int speed;
	GameObject target;
	NavMeshAgent navi;
	string car_num;
	public Vector3 first_position;
	public bool player_target = false;
	
	// Use this for initialization
	void Awake()
	{
		navi = gameObject.GetComponent<NavMeshAgent>();
		first_position = gameObject.transform.position;
		car_num = gameObject.name.Split("_"[0])[1];
		target = GameObject.Find("target_"+car_num);
	}
	void Start () {
		speed = Random.Range(3,10);
		navi.destination = target.transform.position;
		navi.speed = speed;
	}
	// Update is called once per frame
	void LateUpdate () {
		if(player_target == true){
			target = GameObject.Find("Player");
			gameObject.tag = "policeCar";
		}
		else{
			target = GameObject.Find("target_"+car_num);
			gameObject.tag = "car";
		}
		navi.destination = target.transform.position;
		if(GameObject.FindWithTag("gamemanager").GetComponent<gamemanager>().car_move_admit == false)
			navi.speed =0;
		else{
			navi.speed = speed;
		}
	}
}
