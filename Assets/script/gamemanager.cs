using UnityEngine;
using System.Collections;


public class gamemanager : MonoBehaviour {
	public static gamemanager gm;
	public bool connect_load = false;
	public bool new_start_load = false;
	bool string_loading = false;
	public int scene_num;
	public bool loading_camera_on = false;
	AsyncOperation async;
	public string get_load_name = null;
	public bool collision_stage = false;
	public GameObject _obj;
	public bool car_move_admit = true;
	public bool car_target_player = false;
	public int stage_admitTime;
	public bool soundOff = false;
	public bool police_sound = false;
	public bool car_crash = false;
	void Awake()
	{
		car_crash = false;
		DontDestroyOnLoad(this);
		gm = this;
		if(Application.loadedLevelName.Split("_"[0])[1] != "road"){
			if(Application.loadedLevelName == "scene_1")
				stage_admitTime = 5*60;
			if(Application.loadedLevelName == "scene_2")
				stage_admitTime = 3*60+30;
			if(Application.loadedLevelName == "scene_3")
				stage_admitTime = 3*60+30;
			if(Application.loadedLevelName == "scene_4")
				stage_admitTime = 3*60;
			if(Application.loadedLevelName == "scene_5")
				stage_admitTime = 1*60;
			if(Application.loadedLevelName == "scene_6")
				stage_admitTime = 6*60;
			if(Application.loadedLevelName == "scene_7")
				stage_admitTime = 3*60;
			if(Application.loadedLevelName == "scene_8")
				stage_admitTime = 3*60;
			if(Application.loadedLevelName == "scene_9")
				stage_admitTime = 3*60;
			if(Application.loadedLevelName == "scene_10")
				stage_admitTime = 3*60;
		}
		else{
			if(Application.loadedLevelName == "scene_road_1")
				stage_admitTime = 1*60;
			if(Application.loadedLevelName == "scene_road_2")
				stage_admitTime = 1*60;
			if(Application.loadedLevelName == "scene_road_5")
				stage_admitTime = 1*60;
			if(Application.loadedLevelName == "scene_road_6")
				stage_admitTime = 1*60;
			if(Application.loadedLevelName == "scene_road_8")
				stage_admitTime = 1*60;
		}
	}
	// Use this for initialization
	void Start() {
		scene_num = PlayerPrefs.GetInt("Scene_number");
		GameObject[] objlist;
	   	objlist = GameObject.FindGameObjectsWithTag("gamemanager");
		if( objlist.Length > 1 ){
			Destroy( gameObject );
			return;
		}
	}
	
	// Update is called once per frame
	void Update () {
		if(PlayerPrefs.GetInt("Scene_number") != scene_num)
			scene_num = PlayerPrefs.GetInt("Scene_number");
		//PlayerPrefs.SetInt("Scene_number",gamemanager.gm.scene_num);
		
		//if(Application.platform == RuntimePlatform.Android)
		{
			Screen.sleepTimeout = SleepTimeout.NeverSleep;
			if(Input.GetKey(KeyCode.Escape) && Application.loadedLevelName == "main_menu")
				Application.Quit();
			
			if(new_start_load == true){
				//PlayerPrefs.DeleteAll();
				make_loading("scene_1");
				new_start_load = false;
			}
			else if(connect_load == true)
			{
				connect_load = false;
				make_loading("scene_"+scene_num);
			}
			
			if(get_load_name != "" && string_loading == false)
			{
				string_loading = true;
				make_loading(get_load_name);
			}
			
			if(async != null)
				end_loading();
			
		}
		if(loading_camera_on == true)
		{
			loading_camera LC = GameObject.FindWithTag("loading_camera").GetComponent<loading_camera>();
			if(LC != null)
				LC.camera.enabled = true;
		}
		else
		{
			loading_camera LC = GameObject.FindWithTag("loading_camera").GetComponent<loading_camera>();
			if(LC != null)
				LC.camera.enabled = false;
		}
	}
	
	void make_loading(string scene)
	{
		if(async == null)
			async = Application.LoadLevelAsync(scene);
		StartCoroutine(handleSomething());
		loading_camera_on = true;
	}
	
	void end_loading()
	{
		if(async.isDone){
			loading_camera_on = false;
			string_loading = false;
			async = null;
			get_load_name = "";
		}
	}
	
	IEnumerator handleSomething(){	
		yield return new WaitForSeconds( 1.5f );
	}

}
