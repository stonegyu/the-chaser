using UnityEngine;
using System.Collections;

public class intro : MonoBehaviour {
	public static intro Intro;
	// Use this for initialization
	
	void Awake()
	{
		Intro = this;
	}
	void Start () {
		iTween.MoveTo(gameObject,iTween.Hash("x",1,"y",0,"time",1f,"delay",2f,"easetype","linear"));
		iTween.ScaleTo(gameObject,iTween.Hash("x",2.5f,"y",2,"time",1f,"delay",2f,"easetype","linear"));
		iTween.MoveTo(gameObject,iTween.Hash("x",1,"y",1,"time",1f,"delay",6f,"easetype","easeInQuint"));
		iTween.MoveTo(gameObject,iTween.Hash("x",0,"y",0.1,"time",1f,"delay",10f,"easetype","easeInQuint"));
		iTween.MoveTo(gameObject,iTween.Hash("x",0,"y",1,"time",1f,"delay",14f,"easetype","easeInQuint"));
		iTween.MoveTo(gameObject,iTween.Hash("x",0.5f,"y",0.5f,"time",1f,"delay",18f,"easetype","linear"));
		iTween.ScaleTo(gameObject,iTween.Hash("x",1f,"y",1,"time",1f,"delay",18f,"easetype","linear"));
	}
	
	// Update is called once per frame
	void Update () {
		if(mission_camera.MC.mission_guilayer.HitTest(Input.mousePosition) != null && mission_camera.MC.mission_guilayer.HitTest(Input.mousePosition).tag == "gameStart" && Input.GetMouseButton(0))
		{
			Destroy(GameObject.FindWithTag("gameStart"));
			Destroy(gameObject);
		}
	
	}
}
