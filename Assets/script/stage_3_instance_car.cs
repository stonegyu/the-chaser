using UnityEngine;
using System.Collections;

public class stage_3_instance_car : MonoBehaviour {
	GameObject car_1;
	float fltime;
	// Use this for initialization
	void Start () {
		car_1 = (GameObject)Resources.Load("stage3car_1");
	
	}
	
	// Update is called once per frame
	void Update () {
		if(mission_move.MM.guiTexture.enabled == false){
			fltime = fltime + Time.deltaTime;
			if(fltime >20)
			{
				fltime =0;
				Instantiate(car_1,gameObject.transform.position,Quaternion.identity);
			}
		}
	}
}
