using UnityEngine;
using System.Collections;

public class clear_logo : MonoBehaviour {
	GUITexture GT;
	// Use this for initialization
	void Start () {
		GT = GetComponent<GUITexture>();
		GT.pixelInset = new Rect(0,0,Screen.width*0.103f,Screen.width*0.103f);
		iTween.ScaleFrom(gameObject,iTween.Hash("x",0.5f,"y",0.5f,"time",0.5f,"delay",0.5f,"easetype","easeInQuint"));
	
	}
	
	// Update is called once per frame
	void Update () {
		if(mission_move.MM.one == false){
			Destroy(gameObject);
		}
	
	}
}
