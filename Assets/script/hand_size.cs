using UnityEngine;
using System.Collections;

public class hand_size : MonoBehaviour {
	float first_position_x;
	float first_position_y;
	GUITexture GT;
	// Use this for initialization
	void Awake()
	{
		GT = gameObject.GetComponent<GUITexture>();
	}
	void Start () {
		first_position_x = gameObject.transform.position.x;
		first_position_y = gameObject.transform.position.y;
		GT.pixelInset = new Rect(-Screen.width/6f,-Screen.height/20f,Screen.width/6.5f,Screen.height/8);
		if(gameObject.name == "left_fab(Clone)")
			iTween.MoveTo(gameObject,iTween.Hash("x",first_position_x+0.05f,"time",0.5f,"loopType","pingPong"));
		if(gameObject.name == "right_fab(Clone)")
			iTween.MoveTo(gameObject,iTween.Hash("x",first_position_x-0.05f,"time",0.5f,"loopType","pingPong"));
		if(gameObject.name == "up_fab(Clone)")
			iTween.MoveTo(gameObject,iTween.Hash("y",first_position_y-0.05f,"time",0.5f,"loopType","pingPong"));
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
