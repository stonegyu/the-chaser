using UnityEngine;
using System.Collections;

public class screen_move : MonoBehaviour {
	public static screen_move SM;
	GameObject main_camera;
	float first_position_x;
	float move_position_x;
	float gap_position_x;
	public bool click = false;
	float first_rotation_y;
	bool one = false;
	float time;
	public bool end_itween = false;
	bool bPause = false;
	// Use this for initialization
	
	void OnPause () {
		
		bPause = true;
	}
	
	void OnResume () {
		
		bPause = false;
	}
	
	void Awake()
	{
		SM = this;
		main_camera = GameObject.FindWithTag("MainCamera");
	}
	void Start () {
		
		GameObject[] objlist;
	   	objlist = GameObject.FindGameObjectsWithTag("Screen_move");
		if( objlist.Length > 1 ){
			Destroy( gameObject );
			return;
		}
	
	}
	void LateUpdate() {
		if(bPause == true)
			return;
		
		if(Input.GetMouseButtonDown(0)){
			if(back_mirror.BM.touch_noting == true && (handle.car_handle.enter_mouse_1 == false && handle.car_handle.enter_mouse_2 == false) && end_itween == false){
				click = true;
			}
			else
				click = false;
		}
		
	}
	// Update is called once per frame
	void Update () {
		if(bPause == true)
			return;
		
		if(end_itween == true)
		{
			time = time +Time.deltaTime;
			if(time > 1)
			{
				end_itween = false;
				time =0;
			}
		}
		gap_position_x = move_position_x - first_position_x;
		if(Input.GetMouseButtonDown(0)){
			first_position_x = Input.mousePosition.x;
		}
		if(Input.GetMouseButton(0)){
			move_position_x = Input.mousePosition.x;
		}
		if(Input.GetMouseButtonUp(0)){
			if(click == true)
			{
				iTween.RotateTo(main_camera,iTween.Hash("y",first_rotation_y,"time",1f,"easetype","spring"));
				end_itween = true;
			}
			click = false;
			gap_position_x=0;
			move_position_x=0;
			first_position_x=0;
			one = false;
		}
		if(button.bt.button_down == false && brake.bk.brake_douwn == false)
		{
			if(click == true){
				if(one == false){
					first_rotation_y = main_camera.transform.eulerAngles.y;
					one = true;
				}
				main_camera.transform.eulerAngles = new Vector3(0,first_rotation_y-gap_position_x/15,0);
			}
		}
	}
}
