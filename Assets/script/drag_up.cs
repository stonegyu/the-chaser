using UnityEngine;
using System.Collections;

public class drag_up : MonoBehaviour {
	float first_position_x;
	GUITexture GT;
	// Use this for initialization
	void Awake()
	{
		GT = gameObject.GetComponent<GUITexture>();
	}
	void Start () {
		first_position_x = gameObject.transform.position.x;
		GT.pixelInset = new Rect(-Screen.width/6f,-Screen.height/20f,Screen.width/6.5f,Screen.height/8);
		iTween.MoveTo(gameObject,iTween.Hash("x",first_position_x+0.2f,"time",0.5f,"loopType","pingPong"));
	}
	
	void Update()
	{
		if(advice_drag.AD == null)
			Destroy(gameObject);
	}
}
