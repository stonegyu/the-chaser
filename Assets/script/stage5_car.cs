using UnityEngine;
using System.Collections;

public class stage5_car : MonoBehaviour {

	int speed;
	GameObject target;
	NavMeshAgent navi;
	string car_num;
	public Vector3 first_position;
	
	// Use this for initialization
	void Awake()
	{
		navi = gameObject.GetComponent<NavMeshAgent>();
		first_position = gameObject.transform.position;
		car_num = gameObject.name.Split("_"[0])[1];
		target = GameObject.Find("target_"+car_num);
	}
	void Start () {
		if(car_num == "1(Clone)")
			speed=10;
		if(car_num == "2(Clone)")
			speed=15;
		if(car_num == "3(Clone)")
			speed=10;
		if(car_num == "4(Clone)")
			speed=15;
		if(car_num == "5(Clone)")
			speed=10;
		if(car_num == "6(Clone)")
			speed=15;
		if(car_num == "7(Clone)")
			speed=10;
		if(car_num == "8(Clone)")
			speed=15;
		navi.destination = target.transform.position;
		navi.speed = speed;
	}
	// Update is called once per frame
	void LateUpdate () {
		navi.destination = target.transform.position;
		if(GameObject.FindWithTag("gamemanager").GetComponent<gamemanager>().car_move_admit == false)
			navi.speed =0;
		else{
			navi.speed = speed;
		}
	
	}
	
	void OnTriggerEnter(Collider other) {
		if(other.transform.gameObject.name == target.name)
			Destroy(gameObject);
	}
}
