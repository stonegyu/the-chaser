using UnityEngine;
using System.Collections;

public class instance_car : MonoBehaviour {
	GameObject line_1,line_2,line_3,line_4,line_5;
	GameObject car_1,car_2,car_3,car_4,car_5;
	int rand_car,rand_line;
	float fltime;
	GameObject car,line;
	// Use this for initialization
	void Start () {
		line_1 = GameObject.Find("line_1");
		line_2 = GameObject.Find("line_2");
		line_3 = GameObject.Find("line_3");
		line_4 = GameObject.Find("line_4");
		line_5 = GameObject.Find("line_5");
		car_1 = (GameObject)Resources.Load("car_1");
		car_2 = (GameObject)Resources.Load("car_2");
		car_3 = (GameObject)Resources.Load("car_3");
		car_4 = (GameObject)Resources.Load("car_4");
		car_5 = (GameObject)Resources.Load("car_5");
	}
	
	// Update is called once per frame
	void Update () {
		fltime = fltime + Time.deltaTime;
		if(fltime >10)
		{
			fltime =0;
			rand_car = Random.Range(0,5);
			rand_line = Random.Range(0,5);
			if(rand_car == 0)
				car = car_1;
			else if(rand_car == 1)
				car = car_2;
			else if(rand_car == 2)
				car = car_3;
			else if(rand_car == 3)
				car = car_4;
			else if(rand_car == 4)
				car = car_5;
			if(rand_line == 0)
				line = line_1;
			else if(rand_line == 1)
				line = line_2;
			else if(rand_line == 2)
				line = line_3;
			else if(rand_line == 3)
				line = line_4;
			else if(rand_line == 4)
				line = line_5;
			Instantiate(car,line.transform.position,Quaternion.identity);
		}
	}
}
