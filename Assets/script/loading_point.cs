using UnityEngine;
using System.Collections;

public class loading_point : MonoBehaviour {
	GUITexture GT;
	public Texture2D[] texture;
	float time;
	// Use this for initialization
	void Start () {
		GT = GetComponent<GUITexture>();
	}
	
	// Update is called once per frame
	void Update () {
		loading_camera LC = GameObject.FindWithTag("loading_camera").GetComponent<loading_camera>();
		if(LC.camera.enabled == true)
		{ 
			time = time + Time.deltaTime;
			int _value = (int)(time/0.1f);
			if(_value ==0)
				GT.texture = texture[0];
			else if(_value ==1)
				GT.texture = texture[1];
			else if(_value ==2)
				GT.texture = texture[2];
			else if(_value ==3)
				GT.texture = texture[3];
			else if(_value ==4)
				GT.texture = texture[4];
			else if(_value ==5)
				GT.texture = texture[5];
			else if(_value ==6)
				GT.texture = texture[6];
			else if(_value ==7)
				GT.texture = texture[7];
			if(_value >=8)
				time =0;
		}
	
	}
}
