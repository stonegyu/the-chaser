using UnityEngine;
using System.Collections;

public class stage1MissionExit : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	void OnTriggerExit(Collider other) {
		if(other.transform.gameObject.name == "Player"){
			gamemanager.gm.collision_stage = true;
			gamemanager.gm._obj = gameObject;
		}
	}
}
