using UnityEngine;
using System.Collections;

public class pointer : MonoBehaviour {
	public static pointer PT;
	Texture2D point;
	Vector2 pivot;
	Rect pointer_rect;
	float angle;
	public bool bPause = false;
	// Use this for initialization
	
	void OnPause () {
		
		bPause = true;
	}
	
	void OnResume () {
		
		bPause = false;
	}
	
	void Awake()
	{
		PT = this;
	}
	void Start () {
		point = (Texture2D)Resources.Load("pointer");
		pointer_rect = new Rect(0,0,Screen.width*0.025f,Screen.width*0.025f);
		pointer_rect.center = new Vector2(Screen.width*0.297f,Screen.height*0.16f);
	}
	
	// Update is called once per frame
	void Update () {
		if(bPause == true)
			return;
		
		angle +=10;
	}
	
	void OnGUI()
	{
		pivot = new Vector2(pointer_rect.center.x-0.1f,pointer_rect.center.y-0.1f);
		GUIUtility.RotateAroundPivot(angle,pivot);
		loading_camera LC = GameObject.FindWithTag("loading_camera").GetComponent<loading_camera>();
		if(LC != null && LC.camera.enabled == false && mission_move.MM.guiTexture.enabled == false)
			GUI.DrawTexture(pointer_rect,point);
	}
}
