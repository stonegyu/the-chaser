using UnityEngine;
using System.Collections;

public class brake : MonoBehaviour {
	public static brake bk;
	public bool brake_douwn = false;
	GUITexture GT;
	bool bPause = false;
	// Use this for initialization
	void Awake()
	{
		bk = this;
		GT = gameObject.transform.GetComponent<GUITexture>();
		iTween.FadeTo(gameObject,iTween.Hash("alpha",0.3f));
	}
	void Start () {
		GT.pixelInset = new Rect(-Screen.width/2.15f,-Screen.height/2+Screen.height/4.5f,Screen.width/8,Screen.height/5);
		GameObject[] objlist;
	   	objlist = GameObject.FindGameObjectsWithTag("brake");
		if( objlist.Length > 1 ){
			Destroy( gameObject );
			return;
		}
		
	}
	
	void OnPause () {
		
		bPause = true;
	}
	
	void OnResume () {
		
		bPause = false;
	}
	
	// Update is called once per frame
	void Update () {
		if(bPause == true)
			return;
		
		if(camera_move.CM.main_GUILayer.HitTest(Input.mousePosition) != null && camera_move.CM.main_GUILayer.HitTest(Input.mousePosition).tag == "brake" 
			&& Input.GetMouseButton(0) && screen_move.SM.click == false)
		{
			GT.texture = (Texture)Resources.Load("brake_pushed");
			brake_douwn = true;
		}
		else
		{
			brake_douwn = false;
			GT.texture = (Texture)Resources.Load("brake");
		}
	
	}
}
