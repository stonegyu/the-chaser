using UnityEngine;
using System.Collections;

public class move : MonoBehaviour {
	public static move mv;
	public Vector3 rotate = Vector3.zero;
	//Vector3 trans;
	Vector3 rigid_go;
	Vector3 rigid_back;
	//Vector3 collision_enter;
	Quaternion target;
	public bool vel_zero = false;
	public bool left_go = false;
	public bool right_go = false;
	float one_rotation_y;
	Vector3 dir = Vector3.zero;
	float test_horizontal;
	bool bPause = false;
	bool pump_admit = false;
	float pump_count=0;
	float angle;
	float angleRotation;
	float maximumRotation;
	bool befor_handle = false;
	float rotate_z;
	int rotateMaxspeed;
	
	void OnPause () {
		
		bPause = true;
	}
	
	void OnResume () {
		
		bPause = false;
	}
	
	void Awake()
	{
		mv = this;
	}
	
	void Start () {
		
		GameObject[] objlist;
	   	objlist = GameObject.FindGameObjectsWithTag("cube");
		if( objlist.Length > 1 ){
			Destroy( gameObject );
			return;
		}
		
		maximumRotation = 2;
		if(Application.loadedLevelName.Split("_"[0])[1] == "road" || Application.loadedLevelName == "scene_4")
			rotateMaxspeed = 25;
		else
			rotateMaxspeed = 5;
	}
	
	void Update() {
		if(bPause == true){
			rigidbody.velocity = new Vector3(0,0,0);
			return;
		}
		
		rotateAngle();
		if(Application.isEditor){
			test_horizontal = Input.GetAxis("Horizontal");
			angle = test_horizontal;
			if(test_horizontal>0)
			{
				if(back_mirror.BM.back_touched ==false){
					right_go = true;
					left_go = false;
				}
				else
				{
					left_go = true;
					right_go = false;
				}
			}
			else if(test_horizontal<0)
			{
				if(back_mirror.BM.back_touched ==false){
					left_go = true;
					right_go = false;
				}
				else
				{
					right_go = true;
					left_go = false;
				}
			}
			else if(test_horizontal ==0){
				right_go = false;
				left_go = false;
			}
			move_go();
			if(button.bt.button_down == false)
			{
				if(rigidbody.velocity == new Vector3(0,0,0))
					button.bt.fltime =0;
				if(button.bt.fltime > 0)
					button.bt.fltime = button.bt.fltime - 0.05f;
				if(button.bt.fltime <0)
					button.bt.fltime =0;
				right_go = false;
				left_go = false;
			}
			brake_button();
		}
		else if(Application.platform == RuntimePlatform.Android){
			main_tilt();
			if(button.bt.button_down == false && gameObject.rigidbody.velocity.magnitude != 0)
			{
				move_go();
			}
			if(button.bt.button_down == true){
				move_go();
			}
			else
			{
				if(rigidbody.velocity == new Vector3(0,0,0))
					button.bt.fltime =0;
				if(button.bt.fltime > 0)
					button.bt.fltime = button.bt.fltime - 0.05f;
				if(button.bt.fltime <0)
					button.bt.fltime =0;
				right_go = false;
				left_go = false;
			}
			brake_button();
		}
		
		if(pump_admit == true)
			pump_count += Time.deltaTime;
		if(pump_count > 5)
		{
			pump_admit = false;
			pump_count =0;
		}
	}
	
	void OnCollisionEnter(Collision collision) {
		if(bPause == false){
			if(pump_admit == false){
				iTween.ShakeRotation(gameObject,iTween.Hash("z",10,"time",1f,"easetype","spring"));
				Handheld.Vibrate();
				pump_admit = true;
				GameObject.FindWithTag("gamemanager").GetComponent<gamemanager>().car_crash = true;
				if(collision.transform.gameObject.tag == "car" || collision.transform.gameObject.tag == "policeCar"){
					GameObject.Find("time").GetComponent<countDown>().stage_time -=60;
				}
				else
					GameObject.Find("time").GetComponent<countDown>().stage_time -=10;
			}
		}
	}
	
	void OnCollisionExit(Collision collision) {
		if(angle == 0)
		{
			iTween.RotateTo(gameObject,iTween.Hash("z",0,"time",0.5f,"delay",1f,"easetype","spring"));
		} 
	}
	
	void main_tilt()
	{
		dir.x = -Input.acceleration.y;        
		dir.z = Input.acceleration.x;        
		if (dir.sqrMagnitude > 1)            
			dir.Normalize();                
		dir *= Time.deltaTime;        
		rotate = dir*10;
		angle = rotate.x;
		if(rotate.x > 0){
			if(back_mirror.BM.back_touched ==false){
				right_go = true;
				left_go = false;
			}
			else
			{
				left_go = true;
				right_go = false;
			}
		}
		else if(rotate.x < -0){
			if(back_mirror.BM.back_touched ==false){
				left_go = true;
				right_go = false;
			}
			else
			{
				right_go = true;
				left_go = false;
			}
		}
			else{
				right_go = false;
				left_go = false;
			}
	}
	
	void move_go()
	{
		if(right_go == true){
			angleRotation = gameObject.rigidbody.velocity.magnitude*2f/rotateMaxspeed*(angle*3);
			if(angleRotation > maximumRotation)
				angleRotation = maximumRotation;
			transform.eulerAngles = new Vector3(0,transform.eulerAngles.y+angleRotation,transform.eulerAngles.z);
		}
		else if(left_go == true){
			angleRotation = gameObject.rigidbody.velocity.magnitude*2f/rotateMaxspeed*(-angle*3);
			if(angleRotation > maximumRotation)
				angleRotation = maximumRotation;
			transform.eulerAngles = new Vector3(0,transform.eulerAngles.y-angleRotation,transform.eulerAngles.z);
		}
		rigid_go = transform.eulerAngles;
		if(rigid_go.y ==0 && handle.car_handle.rear == false){
			if(befor_handle == true){
				if(rigidbody.velocity.magnitude >= 1)
					rigidbody.velocity = Vector3.Lerp(rigidbody.velocity,new Vector3(0,0,0),0.03f);
				else{
					befor_handle = false;
					button.bt.fltime =0;
				}
			}
			else
				rigidbody.velocity = Vector3.Lerp(rigidbody.velocity,new Vector3(0,0,(button.bt.fltime*2)),Time.deltaTime);
		}
		else if(rigid_go.y ==0 && handle.car_handle.rear == true){
			if(befor_handle == false)
			{
				if(rigidbody.velocity.magnitude >= 1)
					rigidbody.velocity = Vector3.Lerp(rigidbody.velocity,new Vector3(0,0,0),0.03f);
				else{
					befor_handle = true;
					button.bt.fltime =0;
				}
			}
			else
				rigidbody.velocity = Vector3.Lerp(rigidbody.velocity,new Vector3(0,0,-(button.bt.fltime*2)),Time.deltaTime);
		}
		else if(rigid_go.y !=0 && handle.car_handle.rear == false){
			if(befor_handle == true){
				if(rigidbody.velocity.magnitude >= 1)
					rigidbody.velocity = Vector3.Lerp(rigidbody.velocity,new Vector3(0,0,0),0.03f);
				else{
					befor_handle = false;
					button.bt.fltime =0;
				}
			}
			else
				rigidbody.velocity = Vector3.Lerp(rigidbody.velocity,new Vector3((rigidbody.velocity.x+(button.bt.fltime)*Mathf.Sin(rigid_go.y*3.14f/180)),0,(rigidbody.velocity.z+button.bt.fltime*Mathf.Cos(rigid_go.y*3.14f/180))),Time.deltaTime);
		}
		else if(rigid_go.y !=0 && handle.car_handle.rear == true){
			if(befor_handle == false)
			{
				if(rigidbody.velocity.magnitude >= 1)
					rigidbody.velocity = Vector3.Lerp(rigidbody.velocity,new Vector3(0,0,0),0.03f);
				else{
					befor_handle = true;
					button.bt.fltime =0;
				}
			}
			else
				rigidbody.velocity = Vector3.Lerp(rigidbody.velocity,new Vector3(-(button.bt.fltime*2)*(Mathf.Sin(rigid_go.y*3.14f/180)),0,-(button.bt.fltime*2)*(Mathf.Cos(rigid_go.y*3.14f/180))),Time.deltaTime);
		}
	}
	
	void brake_button()
	{
		if(brake.bk.brake_douwn == true){
			if(rigidbody.velocity.sqrMagnitude != 0)
				rigidbody.velocity = Vector3.Lerp(rigidbody.velocity,new Vector3(0,0,0),0.1f);
			if(rigidbody.velocity.sqrMagnitude <= 0.01)
				vel_zero = true;
			if(rigidbody.velocity == new Vector3(0,0,0))
				button.bt.fltime =0;
			if(button.bt.fltime > 0)
				button.bt.fltime = button.bt.fltime - 0.1f;
			if(button.bt.fltime <0)
				button.bt.fltime =0;
		}
		else
			vel_zero = false;
	}
	
	void rotateAngle()
	{
		if(angle >0)
			rotate_z = Mathf.Lerp(rotate_z,-angleRotation*10,Time.deltaTime);
		else if(angle <0)
			rotate_z = Mathf.Lerp(rotate_z,angleRotation*10,Time.deltaTime);
		else
			rotate_z = Mathf.Lerp(rotate_z,0,Time.deltaTime);
		if(rigidbody.velocity.magnitude < 0.1f)
			rotate_z = Mathf.Lerp(rotate_z,0,Time.deltaTime);
		transform.eulerAngles = new Vector3(transform.eulerAngles.x,transform.eulerAngles.y,rotate_z);
	}
}
