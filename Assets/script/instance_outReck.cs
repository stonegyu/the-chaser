using UnityEngine;
using System.Collections;

public class instance_outReck : MonoBehaviour {
	public static instance_outReck ins_out;
	GameObject exit;
	public bool exit_pushed = false;
	// Use this for initialization
	void Awake()
	{
		ins_out = this;
	}
	void Start () {
		exit = (GameObject)Resources.Load("exit");
		
		GameObject[] objlist;
	   	objlist = GameObject.FindGameObjectsWithTag("out_rect");
		if( objlist.Length > 1 ){
			Destroy( gameObject );
			return;
		}
	}
	
	// Update is called once per frame
	void Update () {
		//if(Application.platform == RuntimePlatform.Android)
		{
			if(Input.GetKey(KeyCode.Escape) && gameover.over.end_game == false && exit_pushed == false){
				make_exit();
				exit_pushed = true;
			}
			//if(Input.GetKey(KeyCode.Escape) && gameover.over.end_game == false && exit_pushed == true){
			//	re_move();
			//}
			
			if(exit_pushed == true){
				if(mission_camera.MC.mission_guilayer.HitTest(Input.mousePosition) != null && mission_camera.MC.mission_guilayer.HitTest(Input.mousePosition).tag == "yes" && Input.GetMouseButton(0)){
					gamemanager gm = GameObject.FindWithTag("gamemanager").GetComponent<gamemanager>();
					gm.get_load_name = "main_menu";
					//Time.timeScale = 1.0f;
				}
				else if(mission_camera.MC.mission_guilayer.HitTest(Input.mousePosition) != null && mission_camera.MC.mission_guilayer.HitTest(Input.mousePosition).tag == "no" && Input.GetMouseButton(0)){
					re_move();
				}
			}
		}
	
	}
	
	void make_exit()
	{
		stop_move();
		//Time.timeScale = 0.0f;
		if(GameObject.FindWithTag("exit") == null)
			Instantiate(exit,new Vector3(0,0,0),Quaternion.identity);
		else{
			re_move();
		}
	}
	
	public void stop_move()
	{
		GameObject[] gameobjects = FindObjectsOfType(typeof(GameObject)) as GameObject[];        
		foreach (GameObject gameObj in gameobjects) {   		
			if( gameObj.name == "button" || gameObj.name == "brake" || gameObj.name == "pointer" || gameObj.name == "handle"
				|| gameObj.name == "time" || gameObj.name == "arrow" || gameObj.name == "Screen_move"
				|| gameObj.name == "emty" || gameObj.name == "Player")
				gameObj.SendMessage( "OnPause", 1f );        
		}
	}
	
	public void re_move()
	{
		exit_pushed = false;
		Destroy(GameObject.FindWithTag("exit"));
		GameObject[] gameobjects = FindObjectsOfType(typeof(GameObject)) as GameObject[];        
		foreach (GameObject gameObj in gameobjects) {   		
			if( gameObj.name == "button" || gameObj.name == "brake" || gameObj.name == "pointer" || gameObj.name == "handle"
				|| gameObj.name == "time" || gameObj.name == "arrow" || gameObj.name == "Screen_move"
				|| gameObj.name == "emty" || gameObj.name == "Player")
				gameObj.SendMessage( "OnResume", 1f );        
		}
		//Time.timeScale = 1.0f;
	}
}
