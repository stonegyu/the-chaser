using UnityEngine;
using System.Collections;

public class mini_target : MonoBehaviour {
	string targetName;
	// Use this for initialization
	void Start () {
		targetName = gameObject.name.Split("_"[0])[1];
	
	}
	
	// Update is called once per frame
	void Update () {
		if(GameObject.Find(targetName+"_line").renderer.enabled == false)
			gameObject.renderer.enabled = false;
		else if(GameObject.Find(targetName+"_line").renderer.enabled == true)
			gameObject.renderer.enabled = true;
	
	}
}
