using UnityEngine;
using System.Collections;

public class infomation : MonoBehaviour {

	GUITexture GT;
	// Use this for initialization
	void Awake()
	{
		GT= gameObject.GetComponent<GUITexture>();
	}
	void Start () {
		GT.pixelInset = new Rect(Screen.width/4f,-Screen.height/2.7f,Screen.width/4,Screen.height/4);
		iTween.MoveFrom(gameObject,iTween.Hash("x",-0.5f,"delay",1.5f,"time",2f,"easetype","easeOutElastic"));
		GameObject.Find("Camera").camera.enabled = false;
	}
	
	// Update is called once per frame
	void Update () {
		if(main_menu_camera.MC.main_GUILayer.HitTest(Input.mousePosition) != null && main_menu_camera.MC.main_GUILayer.HitTest(Input.mousePosition).tag == "infomation" 
			&& Input.GetMouseButtonDown(0))
		{
			GameObject.Find("Camera").camera.enabled = true;
		}
	
	}
}
