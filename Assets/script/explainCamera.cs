using UnityEngine;
using System.Collections;

public class explainCamera : MonoBehaviour {
	Vector3 first_position;
	Vector3 first_rotation;
	bool one = false;
	bool one2 = false;
	// Use this for initialization
	void Start () {
		first_position = gameObject.transform.position;
		first_rotation = gameObject.transform.eulerAngles;
	}
	
	// Update is called once per frame
	void Update () {
		if(GameObject.FindWithTag("stage2Explain") != null){
			if(GameObject.Find("police_3_mission_police_1") == null)
			{
				iTween.MoveTo(gameObject,iTween.Hash("x",first_position.x,"y",first_position.y,"z",first_position.z,"time",1f,"easetype","easeOutQuart"));
				iTween.RotateTo(gameObject,iTween.Hash("x",first_rotation.x,"y",first_rotation.y,"z",first_rotation.z,"time",1f,"easetype","linear"));
			}
			
			if(GameObject.Find("main_2_mission_main_1") == null && GameObject.Find("police_3_mission_police_1") != null && one2 == false)
			{
				gameObject.camera.enabled = true;
				one2 = true;
				if(GameObject.FindWithTag("car") == null){
					iTween.MoveTo(gameObject,iTween.Hash("x",GameObject.FindWithTag("policeCar").transform.position.x,"y",50,"z",GameObject.FindWithTag("policeCar").transform.position.z,"time",1f,"easetype","easeOutQuart"));
				}
				else if(GameObject.FindWithTag("car") != null)
					iTween.MoveTo(gameObject,iTween.Hash("x",GameObject.FindWithTag("car").transform.position.x,"y",50,"z",GameObject.FindWithTag("car").transform.position.z,"time",1f,"easetype","easeOutQuart"));
				iTween.RotateTo(gameObject,iTween.Hash("x",90,"y",0,"z",0,"time",1f,"easetype","linear"));
			}
			one = true;
		}
		else
			gameObject.camera.enabled = false;
		
		if(one == true && GameObject.FindWithTag("stage2Explain") == null)
		{
			Destroy(gameObject);
		}
	}
}
