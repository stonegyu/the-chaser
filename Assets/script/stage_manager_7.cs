using UnityEngine;
using System.Collections;

public class stage_manager_7 : MonoBehaviour {

	public static stage_manager_7 stage;
	GameObject mission1Conver;
	bool mission_1 = false;
	int rand_num;
	int real_num;
	GameObject gard;
	void Awake()
	{
		stage = this;
		if(Application.loadedLevelName == "scene_7"){
			PlayerPrefs.SetInt("Scene_number",7);
		}
		
		if(Application.loadedLevelName == "scene_8"){
			PlayerPrefs.SetInt("Scene_number",8);
			GameObject.FindWithTag("mission").GetComponent<mission_move>().guiTexture.enabled = true;
			GameObject.FindWithTag("mission").GetComponent<mission_move>().guiTexture.texture = (Texture)Resources.Load("Mission(Stage1)");
		}
		
	}
	// Use this for initialization
	void Start () {
		if(Application.loadedLevelName == "scene_8"){
			gard = (GameObject)Resources.Load("gard");
			rand_num = Random.Range(1,3);
			if(rand_num == 1){
				GameObject.Find("out_2").collider.isTrigger = true;
				real_num = 2;
			}
			else{
				GameObject.Find("out_1").collider.isTrigger = true;
				real_num = 1;
			}
			if(rand_num == 1)
			{
				Instantiate(gard,new Vector3(187,-0.5f,18),Quaternion.identity);
			}
			else
			{
				Instantiate(gard,new Vector3(187,-0.5f,65),Quaternion.identity);
			}
		}
		StartCoroutine(handleSomething());
	}
	
	// Update is called once per frame
	void Update () {
		if(mission_move.MM != null && instance_outReck.ins_out != null && GameObject.FindWithTag("gamemanager").GetComponent<gamemanager>().loading_camera_on == false && instance_outReck.ins_out.exit_pushed == false &&
			gameover.over.end_game == false && mission_move.MM.guiTexture.enabled == false && GameObject.FindWithTag("one") == null){
			//Time.timeScale = 1.0f;
			GameObject.FindWithTag("gamemanager").GetComponent<gamemanager>().car_move_admit = true;
			pointer.PT.bPause = false;
			if(instance_outReck.ins_out != null)
				instance_outReck.ins_out.re_move();
		}
		else{
			GameObject.FindWithTag("gamemanager").GetComponent<gamemanager>().car_move_admit = false;
			//Time.timeScale =0.0f;
			if(instance_outReck.ins_out != null)
				instance_outReck.ins_out.stop_move();
		}
		
		if(Application.loadedLevelName == "scene_7"){
			if(mission_move.MM.guiTexture.enabled == false)
			{	
				if(gamemanager.gm._obj != null && gamemanager.gm._obj.name == "house_target" && gamemanager.gm.collision_stage == true && mission_1 == false)
				{
					gamemanager.gm.collision_stage = false;
					mission_1 = true;
					gamemanager gm = GameObject.FindWithTag("gamemanager").GetComponent<gamemanager>();
					gm.get_load_name = "scene_8";
				}
			}
		}
		
		if(Application.loadedLevelName == "scene_8"){
			if(mission_move.MM.guiTexture.enabled == false)
			{	
				if(gamemanager.gm._obj != null && gamemanager.gm._obj.name == "out_"+real_num && gamemanager.gm.collision_stage == true)
				{
					gamemanager.gm.collision_stage = false;
					gamemanager gm = GameObject.FindWithTag("gamemanager").GetComponent<gamemanager>();
					gm.get_load_name = "scene_road_8";
				}
			}
		}
		
		if(Application.loadedLevelName == "scene_road_8"){
			if(gamemanager.gm._obj != null && gamemanager.gm._obj.name == "target" && gamemanager.gm.collision_stage == true)
			{
				gamemanager.gm.collision_stage = false;
				GameObject.FindWithTag("gamemanager").GetComponent<gamemanager>().get_load_name = "scene_9";
			}
		}
	
	}
	
	IEnumerator handleSomething()
	{
		yield return new WaitForSeconds( 0.5f );
		GameObject.FindWithTag("gamemanager").GetComponent<gamemanager>().car_target_player = false;
	}
}
