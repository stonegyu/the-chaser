using UnityEngine;
using System.Collections;

public class stage6ChaserOn : MonoBehaviour {
	string num;
	bool enter = false;
	float first_distance,last_distance;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	
	void OnTriggerEnter(Collider other) {
		if(other.transform.gameObject.name == "Player" && gameObject.name.Split("_"[0])[2] == "chaserOn" &&
			GameObject.Find("car_"+gameObject.name.Split("_"[0])[1]).GetComponent<stage6CarMove>().player_target == false && enter == false)
		{
			num = gameObject.name.Split("_"[0])[1];
			enter = true;
			first_distance = distance(GameObject.Find("Player").transform.position,GameObject.Find("road_"+num).transform.position);
		}
		else if(other.transform.gameObject.name == "Player" && gameObject.name.Split("_"[0])[2] == "chaserOn" &&
			GameObject.Find("car_"+gameObject.name.Split("_"[0])[1]).GetComponent<stage6CarMove>().player_target == true && enter == false)
		{
			num = gameObject.name.Split("_"[0])[1];
			enter = true;
			first_distance = distance(GameObject.Find("Player").transform.position,GameObject.Find("road_"+num).transform.position);
		}
		
		if(other.transform.gameObject.name == "Player" && gameObject.name.Split("_"[0])[2] == "chaserOff" &&
			GameObject.Find("car_"+gameObject.name.Split("_"[0])[1]).GetComponent<stage6CarMove>().player_target == true && enter == false)
		{
			num = gameObject.name.Split("_"[0])[1];
			enter = true;
			first_distance = distance(GameObject.Find("Player").transform.position,GameObject.Find("road_"+num).transform.position);
		}
		else if(other.transform.gameObject.name == "Player" && gameObject.name.Split("_"[0])[2] == "chaserOff" &&
			GameObject.Find("car_"+gameObject.name.Split("_"[0])[1]).GetComponent<stage6CarMove>().player_target == false && enter == false)
		{
			num = gameObject.name.Split("_"[0])[1];
			enter = true;
			first_distance = distance(GameObject.Find("Player").transform.position,GameObject.Find("road_"+num).transform.position);
		}
	}
	
	void OnTriggerExit(Collider other) {
		if(other.transform.gameObject.name == "Player" && gameObject.name.Split("_"[0])[2] == "chaserOn" && gameObject.name.Split("_"[0])[1] == num &&
			GameObject.Find("car_"+gameObject.name.Split("_"[0])[1]).GetComponent<stage6CarMove>().player_target == false && enter == true)
		{
			last_distance = distance(GameObject.Find("Player").transform.position,GameObject.Find("road_"+num).transform.position);
			if(last_distance <= first_distance){
				GameObject.Find("car_"+gameObject.name.Split("_"[0])[1]).GetComponent<stage6CarMove>().player_target = true;
				enter = false;
			}
			else
				enter = false;
		}
		else if(other.transform.gameObject.name == "Player" && gameObject.name.Split("_"[0])[2] == "chaserOn" && gameObject.name.Split("_"[0])[1] == num &&
			GameObject.Find("car_"+gameObject.name.Split("_"[0])[1]).GetComponent<stage6CarMove>().player_target == true && enter == true)
		{
			last_distance = distance(GameObject.Find("Player").transform.position,GameObject.Find("road_"+num).transform.position);
			if(last_distance >= first_distance){
				GameObject.Find("car_"+gameObject.name.Split("_"[0])[1]).GetComponent<stage6CarMove>().player_target = false;
				enter = false;
			}
			else
				enter = false;
		}
		
		
		if(other.transform.gameObject.name == "Player" && gameObject.name.Split("_"[0])[2] == "chaserOff" &&
			GameObject.Find("car_"+gameObject.name.Split("_"[0])[1]).GetComponent<stage6CarMove>().player_target == true && enter == true)
		{
			last_distance = distance(GameObject.Find("Player").transform.position,GameObject.Find("road_"+num).transform.position);
			if(last_distance >= first_distance){
				GameObject.Find("car_"+gameObject.name.Split("_"[0])[1]).GetComponent<stage6CarMove>().player_target = false;
				enter = false;
			}
			else
				enter = false;
		}
		else if(other.transform.gameObject.name == "Player" && gameObject.name.Split("_"[0])[2] == "chaserOff" &&
			GameObject.Find("car_"+gameObject.name.Split("_"[0])[1]).GetComponent<stage6CarMove>().player_target == false && enter == true)
		{
			last_distance = distance(GameObject.Find("Player").transform.position,GameObject.Find("road_"+num).transform.position);
			if(last_distance <= first_distance){
				GameObject.Find("car_"+gameObject.name.Split("_"[0])[1]).GetComponent<stage6CarMove>().player_target = true;
				enter = false;
			}
			else
				enter = false;
		}
	}
	
	float distance(Vector3 player, Vector3 road)
	{
		float distan;
		distan = (player.x - road.x)*(player.x - road.x) + (player.z - road.z)*(player.z - road.z);
		return distan;
	}
}
