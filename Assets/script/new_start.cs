using UnityEngine;
using System.Collections;

public class new_start : MonoBehaviour {
	GUITexture GT;
	public GameObject new_rect;
	int scene;
	// Use this for initialization
	void Awake()
	{
		GT= gameObject.GetComponent<GUITexture>();
	}
	void Start () {
		//new_rect = (GameObject)Resources.Load("New_start_rect");
		GT.pixelInset = new Rect(Screen.width/8f,0f,Screen.width/2.5f,Screen.height/5f);
		iTween.MoveFrom(gameObject,iTween.Hash("x",-0.5f,"delay",0.5f,"time",2f,"easetype","easeOutElastic"));
	}
	
	// Update is called once per frame
	void Update () {
		if(main_menu_camera.MC.main_GUILayer.HitTest(Input.mousePosition) != null && main_menu_camera.MC.main_GUILayer.HitTest(Input.mousePosition).tag == "new_start" 
			&& Input.GetMouseButton(0))
		{
			scene = GameObject.FindWithTag("gamemanager").GetComponent<gamemanager>().scene_num;
			if(GameObject.FindWithTag("New_start_rect") == null && (scene != 1 && scene != 0))
				Instantiate(new_rect,new Vector3(0,0,0),Quaternion.identity);
			else if(scene == 1 || scene == 0){
				gamemanager gm = GameObject.FindWithTag("gamemanager").GetComponent<gamemanager>();
				gm.new_start_load = true;
				//gamemanager.gm.new_start_load = true;
			}
		}
	
	}
}
