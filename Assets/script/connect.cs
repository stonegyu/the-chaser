using UnityEngine;
using System.Collections;

public class connect : MonoBehaviour {
	GUITexture GT;
	// Use this for initialization
	void Awake()
	{
		GT= gameObject.GetComponent<GUITexture>();
	}
	void Start () {
		GT.pixelInset = new Rect(Screen.width/8f,-Screen.height/5f,Screen.width/2.5f,Screen.height/5f);
		iTween.MoveFrom(gameObject,iTween.Hash("x",-0.5f,"delay",1f,"time",2f,"easetype","easeOutElastic"));
	}
	
	// Update is called once per frame
	void Update () {
		if(main_menu_camera.MC.main_GUILayer.HitTest(Input.mousePosition) != null && main_menu_camera.MC.main_GUILayer.HitTest(Input.mousePosition).tag == "connect" 
			&& Input.GetMouseButton(0))
		{
			gamemanager gm = GameObject.FindWithTag("gamemanager").GetComponent<gamemanager>();
			gm.connect_load = true;
		}
	
	}
}
