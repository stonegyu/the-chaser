using UnityEngine;
using System.Collections;

public class yes_size : MonoBehaviour {
	GUITexture GT;
	// Use this for initialization
	void Awake()
	{
		GT = gameObject.GetComponent<GUITexture>();
	}
	void Start () {
		if(gameObject.name == "yes")
			GT.pixelInset = new Rect(-Screen.width/4f,-Screen.height/10f,Screen.width/5f,Screen.height/6);
		else
			GT.pixelInset = new Rect(-Screen.width/6f,-Screen.height/20f,Screen.width/6.5f,Screen.height/8);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
