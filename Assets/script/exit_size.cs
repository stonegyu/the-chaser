using UnityEngine;
using System.Collections;

public class exit_size : MonoBehaviour {
	GUITexture GT;
	// Use this for initialization
	void Awake()
	{
		GT = gameObject.GetComponent<GUITexture>();
	}
	void Start () {
		if(gameObject.name == "exit")
		{
			GT.pixelInset = new Rect(-Screen.width/3f,-Screen.height/6f,Screen.width/1.5f,Screen.height/2.5f);
		}
		else
			GT.pixelInset = new Rect(-Screen.width/4.5f,-Screen.height/10f,Screen.width/2,Screen.height/3);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
