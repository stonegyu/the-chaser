using UnityEngine;
using System.Collections;

public class stage6TargetMove : MonoBehaviour {

	string num;
	int count =1;
	Vector3 first_position_target;
	// Use this for initialization
	void Start () {
		num = gameObject.name.Split("_"[0])[1];
		first_position_target = transform.position;
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	void OnTriggerEnter(Collider other) {
		if(other.transform.gameObject.name == "car_"+num)
		{
			count +=1;
			
			if(count %2 == 1)
				transform.position = first_position_target;
			else{
				stage6CarMove CM = GameObject.Find("car_"+num).GetComponent<stage6CarMove>();
				transform.position = new Vector3(CM.first_position.x,-1,CM.first_position.z);
			}
		}
	}
}
