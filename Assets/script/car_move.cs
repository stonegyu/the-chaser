using UnityEngine;
using System.Collections;

public class car_move : MonoBehaviour {
	int speed;
	//bool collision = false;
	//GameObject front_car;
	GameObject target;
	NavMeshAgent navi;
	string car_num;
	public Vector3 first_position;
	bool road = false;
	
	// Use this for initialization
	void Awake()
	{
		navi = gameObject.GetComponent<NavMeshAgent>();
		if(Application.loadedLevelName.Split("_"[0])[1] != "road" && Application.loadedLevelName != "scene_4" &&
			Application.loadedLevelName != "scene_10"){
			first_position = gameObject.transform.position;
			car_num = gameObject.name.Split("_"[0])[1];
			target = GameObject.Find("target_"+car_num);
			road = true;
		}
		else{
			target = GameObject.Find("targetInRoad");
			road = false;
		}
	}
	void Start () {
		if(Application.loadedLevelName == "scene_3")
			speed = 3;
		else if(Application.loadedLevelName == "scene_2")
			speed = 5;
		else if(Application.loadedLevelName == "scene_4" || Application.loadedLevelName == "scene_road_2" ||
			Application.loadedLevelName == "scene_road_5" || Application.loadedLevelName == "scene_road_6" ||
			Application.loadedLevelName == "scene_road_8")
			speed = Random.Range(10,20);
		else if(Application.loadedLevelName == "scene_8")
			speed = 5;
		else if(Application.loadedLevelName == "scene_10")
			speed = Random.Range(10,20);
		else
			speed = Random.Range(3,10);
		navi.destination = target.transform.position;
		navi.speed = speed;
	}
	// Update is called once per frame
	void LateUpdate () {
		if(road == true){
			if(GameObject.FindWithTag("gamemanager").GetComponent<gamemanager>().car_target_player == true){
				gameObject.tag = "policeCar";
				target = GameObject.Find("Player");
			}
			else{
				gameObject.tag = "car";
				target = GameObject.Find("target_"+car_num);
			}
		}
		navi.destination = target.transform.position;
		if(GameObject.FindWithTag("gamemanager").GetComponent<gamemanager>().car_move_admit == false)
			navi.speed =0;
		else{
			navi.speed = speed;
		}
		//if(collision == false){
		//	if(rigidbody.velocity.z < speed)
		//		rigidbody.velocity = new Vector3(0,0,rigidbody.velocity.z+0.5f);
		//}
		//if(collision == true){
		//	rigidbody.velocity = Vector3.Lerp(rigidbody.velocity,new Vector3(0,0,0),Time.deltaTime*10);	
		//}
	}
	/*
	void OnTriggerStay(Collider other) {
		if(other.transform.tag == "car"){
			if(other.transform.gameObject.transform.position.z > transform.position.z){
				collision = true;
				front_car = other.transform.gameObject;
			}
		}
	}
	
	void OnTriggerExit(Collider other) {
		if(other.transform.tag == "car" && other.transform.gameObject == front_car){
			collision = false;
			front_car = null;
		}
	}*/
}
