using UnityEngine;
using System.Collections;

public class lastCar : MonoBehaviour {

	float speed;
	GameObject target;
	NavMeshAgent navi;
	
	// Use this for initialization
	void Awake()
	{
		navi = gameObject.GetComponent<NavMeshAgent>();
		target = GameObject.Find("out");
	}
	void Start () {
		speed =35;
		navi.destination = target.transform.position;
		navi.speed = speed;
	}
	// Update is called once per frame
	void LateUpdate () {
		navi.destination = target.transform.position;
		if(GameObject.FindWithTag("gamemanager").GetComponent<gamemanager>().car_move_admit == false)
			navi.speed =0;
		else{
			navi.speed = speed;
		}
	
	}
	
	void OnTriggerEnter(Collider other) {
		if(other.transform.gameObject.name == "out"){
			Destroy(gameObject);
		}
	}
	
	void OnCollisionEnter(Collision collision) {
		if(collision.transform.gameObject.name == "Player")
		{
			GameObject.FindWithTag("gamemanager").GetComponent<gamemanager>()._obj = gameObject;
			GameObject.FindWithTag("gamemanager").GetComponent<gamemanager>().collision_stage = true;
		}
	}
}
