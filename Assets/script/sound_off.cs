using UnityEngine;
using System.Collections;

public class sound_off : MonoBehaviour {
	GUITexture GT;
	// Use this for initialization
	void Awake()
	{
		GT= gameObject.GetComponent<GUITexture>();
	}
	void Start () {
		GT.pixelInset = new Rect(Screen.width/4f,-Screen.height/1.9f,Screen.width/4,Screen.height/4);
		iTween.MoveFrom(gameObject,iTween.Hash("x",-0.5f,"delay",2f,"time",2f,"easetype","easeOutElastic"));
	
	}
	
	// Update is called once per frame
	void Update () {
		if(main_menu_camera.MC.main_GUILayer.HitTest(Input.mousePosition) != null && main_menu_camera.MC.main_GUILayer.HitTest(Input.mousePosition).tag == "sound" 
			&& Input.GetMouseButtonDown(0))
		{
			if(GameObject.FindWithTag("gamemanager").GetComponent<gamemanager>().soundOff == false){
				GameObject.FindWithTag("gamemanager").GetComponent<gamemanager>().soundOff = true;
			}
			else
			{
				GameObject.FindWithTag("gamemanager").GetComponent<gamemanager>().soundOff = false;
			}
		}
		
		if(GameObject.FindWithTag("gamemanager").GetComponent<gamemanager>().soundOff == false)
			GT.texture = (Texture)Resources.Load("S-OFF");
		else
			GT.texture = (Texture)Resources.Load("S-ON");
	
	}
}
