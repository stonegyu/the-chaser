using UnityEngine;
using System.Collections;

/// <summary>
/// Trivial script that fills the label's contents gradually, as if someone was typing.
/// </summary>

[RequireComponent(typeof(UILabel))]
[AddComponentMenu("NGUI/Examples/Typewriter Effect")]
public class TypewriterEffect : MonoBehaviour
{
	public int charsPerSecond = 40;

	UILabel mLabel;
	string mText;
	int mOffset = 0;
	float mNextChar = 0f;
	int i =0;
	float ftime;
	string num;
	string objName;
	string lastName;
	string characterName;
	
	void Awake()
	{
		num = gameObject.name.Split("_"[0])[1];
		objName = gameObject.name.Split("_"[0])[0];
		lastName = gameObject.name.Split("_"[0])[2];
		characterName = gameObject.name.Split("_"[0])[3];
	}
	
	void Update ()
	{
		if(objName != GameObject.FindWithTag("window").guiTexture.texture.name.Split("_"[0])[1] && gameObject.GetComponent<UILabel>().enabled== true){
			GameObject.FindWithTag("window").guiTexture.texture = (Texture)Resources.Load("conver_"+objName);
		}
		if(characterName != "null" && gameObject.GetComponent<UILabel>().enabled== true){
			if(GameObject.FindWithTag("character").guiTexture.texture == null)
				GameObject.FindWithTag("character").guiTexture.texture = (Texture)Resources.Load("main_1");
			if(GameObject.FindWithTag("character").guiTexture.enabled == false)
				GameObject.FindWithTag("character").guiTexture.enabled = true;
			if(GameObject.FindWithTag("character").guiTexture.texture != null && characterName != GameObject.FindWithTag("character").guiTexture.texture.name.Split("_"[0])[0]){
				character.CT.transfromCharacter();
				character.CT.chage = true;
				character.CT.textureName = characterName+"_"+gameObject.name.Split("_"[0])[4];
			}
			if(characterName == GameObject.FindWithTag("character").guiTexture.texture.name.Split("_"[0])[0] && gameObject.name.Split("_"[0])[4] != GameObject.FindWithTag("character").guiTexture.texture.name.Split("_"[0])[1]){
				GameObject.FindWithTag("character").guiTexture.texture = (Texture)Resources.Load(characterName+"_"+gameObject.name.Split("_"[0])[4]);
			}
		}
		else if(characterName == "null" && gameObject.GetComponent<UILabel>().enabled== true)
			GameObject.FindWithTag("character").guiTexture.enabled = false;
		if(GameObject.FindWithTag("Panel").GetComponent<panel>().showConversation.ToString() == num){
			if (mLabel == null)
			{
				if(lastName == "mission")
					GameObject.FindWithTag("mission").guiTexture.texture = null;
				
				gameObject.GetComponent<UILabel>().enabled = true;
				GameObject.FindWithTag("Panel").GetComponent<panel>().showing = true;
				mLabel = GetComponent<UILabel>();
				mLabel.supportEncoding = false;
				mLabel.symbolStyle = UIFont.SymbolStyle.None;
				mText = mLabel.font.WrapText(mLabel.text, mLabel.lineWidth / mLabel.cachedTransform.localScale.x, mLabel.maxLineCount, false, UIFont.SymbolStyle.None);
			}
			
			if (mOffset < mText.Length)
			{
				if (mNextChar <= Time.time && GameObject.FindWithTag("Panel").GetComponent<panel>().showAll == false)
				{
					charsPerSecond = Mathf.Max(1, charsPerSecond);
	
					// Periods and end-of-line characters should pause for a longer time.
					float delay = 1f / charsPerSecond;
					char c = mText[mOffset];
					if (c == '.' || c == '\n' || c == '!' || c == '?') delay *= 4f;
	
					mNextChar = Time.time + delay;
					mLabel.text = mText.Substring(0, ++mOffset);
				}
				if(GameObject.FindWithTag("Panel").GetComponent<panel>().showAll == true){
					mLabel.text = mText;
					GameObject.FindWithTag("Panel").GetComponent<panel>().showing = false;
					ftime += Time.deltaTime;
					if(ftime > 3){
						GameObject.FindWithTag("Panel").GetComponent<panel>().showConversation +=1;
						GameObject.FindWithTag("Panel").GetComponent<panel>().showAll = false;
						Destroy(gameObject);
					}
				}
				if(GameObject.FindWithTag("Panel").GetComponent<panel>().exit == true)
				{
					GameObject.FindWithTag("Panel").GetComponent<panel>().showAll = false;
					GameObject.FindWithTag("Panel").GetComponent<panel>().exit = false;
					GameObject.FindWithTag("Panel").GetComponent<panel>().showConversation +=1;
					Destroy(gameObject);
				}
			}
			else{ 
				GameObject.FindWithTag("Panel").GetComponent<panel>().showing = false;
				ftime += Time.deltaTime;
				if(ftime > 3){
					GameObject.FindWithTag("Panel").GetComponent<panel>().showConversation +=1;
					Destroy(gameObject);
				}
				if(GameObject.FindWithTag("Panel").GetComponent<panel>().exit == true)
				{
					GameObject.FindWithTag("Panel").GetComponent<panel>().exit = false;
					GameObject.FindWithTag("Panel").GetComponent<panel>().showConversation +=1;
					Destroy(gameObject);
				}
			}
		}
		else
		{
			gameObject.GetComponent<UILabel>().enabled = false;
		}
	}
}